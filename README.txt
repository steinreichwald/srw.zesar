
Diese Web-Applikation implementiert den ZESAR-Standard mit Hilfe von
SOAP 1.1.


Technische Architektur
===========================
Die Applikation ist so ausgelegt, dass sie weitgehend unabhängig vom zentralen
Datenbestand eines Apothekenrechenzentrums betrieben werden kann. Es werden nur
die für ZESAR benötigten Datenfelder in der Datenbank vorgehalten. Die Anbindung
dieser Applikation an den eigentlichen ARZ-Datenbestand erfolgt über ein
separat zu erstellendes Skript, welches die Daten zur Applikation hochlädt.

Theoretisch wäre es sogar möglich, nur die von ZESAR tatsächlich angefragten
Rezepte hochzuladen (da ZESAR ggf. mehrfach nachfragt, falls eine TAN unbekannt
ist). Aus Vereinfachungsgründen ist dies aber derzeit nicht implementiert.

Prinzipiell soll die Applikation so wenig Logik und Berechnungen wie möglich
enthalten. Dies erhöht die Flexibilität des ARZ (einfache Integration) bei der
Verwendung der Daten. Die Applikation berechnet nur die geforderten
Abrufgebühren, alle weiteren Prozessschritte (Stellen der buchhalterischen
Rechnung für ZESAR, Überprüfung der Minderungsgründe, Überprüfung der korrekten
Abrechnung seitens ZESAR) erfolgen extern (ARZ-spezifisch).


Komponenten
===========================

Wir verwenden Pyramid, weil dieses Framework uns kaum beschränkt und auch das
Standard-Framework bei SRW ist. Von zusätzlichen Web-Funktionalitäten
wie z.B. in Django zu finden würde der SOAP-Server praktisch gar nicht
profitieren.

Die Implementierung des SOAP-Teils wird durch soapfish abgedeckt.
