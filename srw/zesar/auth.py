# encoding: utf-8

from __future__ import absolute_import, unicode_literals

import binascii
import logging
import ssl

from repoze.who.api import APIFactory
from repoze.who.interfaces import IIdentifier
from zope.interface import implementer

from .lib.who_passlib_auth import AuthenticationManager, DBAuthenticator
from .models import User

__all__ = [
    'build_who_api',
    'current_username',
]

def auth_manager_from_config(settings, logger=None):
    passlib_config = {}
    for key, value in settings.items():
        if key.startswith('passlib.'):
            passlib_config[key[8:]] = value
    return AuthenticationManager.init_with_config_dict(passlib_config, logger=logger)


def build_who_api(environ, settings, identifier):
    logger = logging.getLogger(__name__)
    identifiers = {
        'soap_basic_auth': ('soap', SOAPBasicAuthIdentifier(logger)),
        'tls': ('tls', TLSCertificateIdentifier('zesar', logger=logger)),
    }
    auth_manager = auth_manager_from_config(settings, logger=logger)
    db_authenticator = DBAuthenticator(User, auth_manager, logger=logger)
    factory = APIFactory(
        identifiers=[identifiers[identifier]],
        authenticators=[('db', db_authenticator)],
        challengers=(),
        mdproviders=(),
        request_classifier=None,
        challenge_decider=None,
        # remote_user_key: do not use default value "REMOTE_USER" so we never
        # (accidentally) use some external (fake?) authentication.
        # Must not be "None" - otherwise repoze.who adds a "None" key in
        # "environ". That leads to (harmless) exceptions in gunicorn.
        remote_user_key='SRW_REMOTE_USER',
        logger=logger
    )
    return factory(environ)


def current_username(environ):
    identity = environ.get('repoze.who.identity')
    return None if (not identity) else identity['login']


@implementer(IIdentifier)
class SOAPBasicAuthIdentifier(object):
    def __init__(self, logger=None):
        self.log = logger

    # IIdentifier
    def identify(self, environ):
        soap_header = environ.get('soap_header')
        if not soap_header:
            return None
        elif not hasattr(soap_header, 'BasicAuth'):
            return None
        basic_auth = soap_header.BasicAuth
        username = getattr(basic_auth, 'Name', None)
        password = getattr(basic_auth, 'Password', None)
        if username and password:
            return {'login': username, 'password': password}
        return None

    # IIdentifier
    def remember(self, environ, identity):
        return None

    # IIdentifier
    def forget(self, environ, identity):
        return None


@implementer(IIdentifier)
class TLSCertificateIdentifier(object):
    def __init__(self, cert_username, logger=None):
        self.cert_username = cert_username
        self.log = logger

    # IIdentifier
    def identify(self, environ):
        pem_cert = environ.get('HTTP_X_SSL_CLIENT_CERT')
        if pem_cert:
            try:
                der_cert = ssl.PEM_cert_to_DER_cert(pem_cert)
                # bcrypt does not allow null bytes in input so we convert the
                # actual bytes into a hex string first.
                hex_cert = binascii.hexlify(der_cert)
            except Exception as e:
                if self.log:
                    self.log.info('received malformed certificate: %r (%r)' % (pem_cert, e))
                return None
            # Theoretically the username could be retrieved from the
            # certificate itself but this "hack"/workaround has two immediate
            # advantages:
            # - Much simpler code as we don't have to parse any TLS certificate
            # - as far as I know there is no guarantee for a stable common name
            #   to be used in the client certificates and we do not control
            #   the issued certificates.
            return {'login': self.cert_username, 'password': hex_cert}
        return None

    # IIdentifier
    def remember(self, environ, identity):
        """
        Return a list of headers suitable for allowing the requesting
        system to remember the identification information (e.g. a
        Set-Cookie header).  Return None if no headers need to be set.
        """
        return None

    # IIdentifier
    def forget(self, environ, identity):
        """
        Return a list of headers suitable for allowing the requesting
        system to forget the identification information (e.g. a
        Set-Cookie header with an expires date in the past).  Return
        None if no headers need to be set.
        """
        return None
