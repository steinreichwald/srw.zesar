
from __future__ import absolute_import

from io import StringIO
import sys

from genshi.template.markup import MarkupTemplate
import pkg_resources
from pyramid.httpexceptions import HTTPForbidden, HTTPNotFound
from pyramid.response import Response
from pyramid.view import view_config
import six
from soapfish import soap11 as SOAP
from soapfish.core import SOAPError, SOAPRequest, SOAPResponse
from soapfish.lib.attribute_dict import AttrDict
from soapfish.soap_dispatch import SOAPDispatcher

from .auth import build_who_api, current_username
from .lib.request_logger import RequestLogger
from .soap.service import ZesarService


def log_request(request, response_code, exc_info=None, extra_data=None, response=None):
    storage_dir = request.registry.settings.get('srw.request_logging_dir')
    if not storage_dir:
        return
    body_fp = request.body_file_seekable
    username = current_username(request.environ) or 'anonymous'
    extra_info = StringIO()
    if extra_data:
        stringified_lines = map(six.text_type, extra_data)
        extra_info.write('\n\n'.join(stringified_lines))
    pem_cert = request.environ.get('HTTP_X_SSL_CLIENT_CERT')
    if pem_cert:
        extra_info.write('\n\n' + pem_cert + '\n\n')
    extra_info.seek(0, 0)
    if isinstance(response, six.string_types):
        response = AttrDict(body=response.encode('utf-8'))
    logger = RequestLogger(request, response_code=response_code, body=body_fp,
        username=username, exc_info=exc_info, extra_info=extra_info,
        response=response, ignore_headers=('X-Ssl-Client-Cert',))
    logger.create_and_store_report(storage_dir)

@view_config(route_name='zesar_service')
def soap_dispatch(request):
    who_api = build_who_api(request.environ, request.registry.settings, identifier='tls')
    identity = who_api.authenticate()
    if not identity:
        rejected = HTTPForbidden('Access to SRW SOAP service requires authentication.')
        log_request(request, rejected.code, response=rejected.detail)
        return rejected

    exc_info = None
    try:
        content_type = 'text/xml; charset="utf-8"'
        soapfish_request = SOAPRequest(request.environ, request.body)

        soap_response = SOAPDispatcher(ZesarService).dispatch(soapfish_request)
        soapxml_body = soap_response.http_content
        response = Response(
            status=soap_response.http_status_code,
            content_type=content_type,
            body=soapxml_body,
        )
    except Exception:
        response = Response(status=000, content_type='text/plain', body='[missing]')
        exc_info = sys.exc_info()
    log_request(request, response.status, response=response.unicode_body, exc_info=exc_info)
    return response


@view_config(route_name='zesar_wsdl')
def serve_wsdl(request):
    vars_ = {
        'srw_zesar_url': request.route_url('zesar_service'),
        'xsd_url': lambda name: request.route_url('zesar_xsd', name=name)
    }
    wsdl_template_string = pkg_resources.resource_string('srw.zesar.soap.static.v1_0', 'ZesarRzService_V1.0.0.wsdl')
    wsdl_template = MarkupTemplate(wsdl_template_string, encoding='utf-8')
    wsdl = wsdl_template.generate(**vars_).render('xml', encoding='utf-8')
    return Response(
        status='200 OK',
        content_type='text/xml',
        body=wsdl,
    )

@view_config(route_name='zesar_xsd')
def serve_xsd(request):
    xsd_filename = request.matchdict['name'] + '.xsd'
    try:
        # pkg_resources checks for a resource named <xsd_filename> but will not
        # recurse into any sub directories (or parent directories) so we are
        # safe against any attacks using '..' or '/'
        xsd_string = pkg_resources.resource_string('srw.zesar.soap.static.v1_0', xsd_filename)
    except IOError:
        return HTTPNotFound()
    return Response(
        status='200 OK',
        content_type='text/xml',
        body=xsd_string,
    )

