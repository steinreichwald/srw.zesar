# encoding: utf-8

from __future__ import absolute_import

import os
import shutil
import tempfile

from pythonic_testcase import *

from srw.zesar.models import User
from srw.zesar.testutil import zesar_demo_cert, zesar_demo_md5, FunctionalTestCase


class RequestLoggingTest(FunctionalTestCase):
    def setUp(self):
        self.tempdir = tempfile.mkdtemp()
        self.extra_settings = {'srw.request_logging_dir': self.tempdir}
        super(RequestLoggingTest, self).setUp()

    def tearDown(self):
        shutil.rmtree(self.tempdir)
        super(RequestLoggingTest, self).tearDown()

    def test_can_log_username_and_certificate_hash(self):
        # TODO: send correct SOAP request
        User.example(username='zesar', password_hash=zesar_demo_md5, is_zesar=True)
        headers = {'X-SSL-CLIENT-CERT': zesar_demo_cert}
        response = self.app.post('/', headers=headers, expect_errors=True)
        assert_equals(500, response.status_int)

        report = self._find_report()
        assert_contains('User: zesar', report)
        assert_contains(zesar_demo_cert, report)
        assert_contains(response.unicode_body, report)

    def test_can_log_requests_with_bad_authentication(self):
        headers = {'X-SSL-CLIENT-CERT': ''}
        response = self.app.post('/', headers=headers, expect_errors=True)
        assert_equals(403, response.status_int)

        report = self._find_report()
        assert_contains('User: anonymous', report)
        assert_not_contains(zesar_demo_cert, report)
        assert_contains('Access to SRW SOAP service requires authentication.', report)

    def _find_report(self):
        for root, dirnames, filenames in os.walk(self.tempdir):
            for filename in filenames:
                report_pathname = os.path.join(root, filename)
                return open(report_pathname, 'r', encoding='utf-8').read()
        self.fail('no report file found')

