# encoding: utf-8

from __future__ import absolute_import

from datetime import date as Date

from pythonic_testcase import *

from srw.zesar.models import Pharmacy, Prescription
from srw.zesar.testutil import DBTestCase


class PrescriptionTest(DBTestCase):
    def test_can_create_example_instance_with_default_data(self):
        prescription = Prescription.example()
        assert_isinstance(prescription, Prescription)
        assert_isinstance(prescription.pharmacy, Pharmacy)

    def test_derives_year_from_delivery_date(self):
        delivery_date = Date(2012, 4, 1)
        prescription = Prescription.example(delivery_date=delivery_date)
        assert_equals(delivery_date.year, prescription.year)

    def test_can_create_example_instance(self):
        pharmacy = Pharmacy.example(pharmacy_id=987654321)
        prescription = Prescription.example(
            pharmacy=pharmacy,
            transaction_number=123456786,
            year=2011,
            special_code=77777,
            delivery_date=Date(2010, 1, 4),
            currency='EUR',
            data={'positions': [{'pzn': 9999042, 'faktor': 2}]}
        )
        assert_isinstance(prescription, Prescription)
        assert_equals(pharmacy, prescription.pharmacy)
        assert_equals(123456786, prescription.transaction_number)
        assert_equals(2011, prescription.year)
        assert_equals(77777, prescription.special_code)
        assert_equals(Date(2010, 1, 4), prescription.delivery_date)
        assert_equals('EUR', prescription.currency)
        assert_length(1, prescription.positions())
        assert_contains((9999042, 2), prescription.positions())

