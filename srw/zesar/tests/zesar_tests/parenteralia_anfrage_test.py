# encoding: utf-8

from __future__ import absolute_import

from datetime import date as Date, datetime as DateTime, timedelta as TimeDelta
import logging

from babel.util import LOCALTZ
from lxml import etree
from pythonic_testcase import *
from soapfish.core import SOAPRequest
from soapfish import xsd
from soapfish.xsd_types import XSDDate

from srw.zesar.models import (Pharmacy, PharmacyPeriod, Prescription,
    RecordedRequest, User)
from srw.zesar.testutil import DBTestCase
from srw.zesar.soap.sendeParenteraliadaten import *
from srw.zesar.soap.service import ZesarService


class ParenteraliaAnfrageTest(DBTestCase):
    def setUp(self):
        super(ParenteraliaAnfrageTest, self).setUp()
        self.method = sendeParenteraliadaten_method
        self.schema = ParenteraliaAnfrageSchema
        self.user = User.example()

    def test_can_return_error_for_unknown_pharmacy(self):
        satz_data = dict(
            tan='123456786',
            apoIK='300300640',
            abgabedatum='2013-10-02',
            recordID=155494
        )
        fehler = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(fehler, FehlerSatz)
        assert_equals('123456786', fehler.tan)
        assert_equals('300300640', fehler.apoIK)
        assert_equals(9200, fehler.fehlercode) # Apotheke unbekannt
        assert_equals('unbekannte Apotheke', fehler.parameter)
        assert_equals(155494, fehler.recordID)
        assert_length(0, RecordedRequest.query.all())

    def test_can_return_parenteralia_data(self):
        pharmacy = Pharmacy.example(
            pharmacy_id=112233445,
            name='Löwenapotheke',
            street='Eichendorffstr. 71',
            postal_code='4454',
            city='Holzhausen',
            country_code='de',
        )
        assert_smaller(len(pharmacy.postal_code), 5,
            message='postal code response in SOAP must have exactly 5 digits, this tests zero-padding')
        prescription = Prescription.example(
            pharmacy=pharmacy,
            transaction_number=123456786,
            year=2014,
            delivery_date=Date(2014, 10, 2),
            special_code=1654987,
            valuation=10031,
            currency='EUR',
            data={'positions': ({'pzn': 9999042, 'faktor': 100},)},
        )
        assert_length(0, RecordedRequest.query.all())
        satz_data = dict(
            tan='123456786',
            apoIK=str(pharmacy.pharmacy_id),
            abgabedatum='2014-10-02',
            recordID=654321
        )
        result = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(result, ParenteraliaSatzAntwort)
        assert_equals('123456786', result.tan)
        assert_equals('1654987', result.sonderkennzeichen)
        assert_equals(10031, result.taxe)
        assert_equals(XSDDate(2014, 10, 2), result.abgabedatum)
        assert_equals('112233445', result.apoIK)
        assert_equals('Löwenapotheke', result.apoName)
        assert_equals('Eichendorffstr. 71', result.apoStrHausnum)
        assert_equals('04454', result.apoPLZ)
        assert_equals('Holzhausen', result.apoOrt)
        assert_equals('de', result.apoLaenderkennzeichen)
        assert_equals('EUR', result.waehrungskennzeichen)
        assert_equals(654321, result.recordID)

        assert_length(1, result.einzelbestandteile)
        data = result.einzelbestandteile[0]
        assert_equals('9999042', data.pzn)
        assert_equals(100, data.faktor)

        recorded_request = RecordedRequest.query.one()
        assert_equals(654321, recorded_request.record_id)
        assert_equals(pharmacy, recorded_request.pharmacy)
        assert_equals(123456786, recorded_request.transaction_number)
        assert_equals(prescription, recorded_request.prescription)
        assert_almost_equals(
            DateTime.now(tz=LOCALTZ),
            recorded_request.request_time,
            max_delta=TimeDelta(seconds=2)
        )

    def test_filters_prescriptions_by_pharmacy(self):
        tan = 123456786
        request_pharmacy_id = 222222222
        pharmacy_1 = Pharmacy.example(pharmacy_id=111111111, name='A')
        pharmacy_2 = Pharmacy.example(pharmacy_id=request_pharmacy_id, name='B')
        Prescription.example(pharmacy=pharmacy_1, transaction_number=tan)
        Prescription.example(pharmacy=pharmacy_2, transaction_number=tan)

        satz_data = dict(
            tan=str(tan),
            apoIK=str(request_pharmacy_id),
            abgabedatum='2014-10-02',
            recordID=123456
        )
        result = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(result, ParenteraliaSatzAntwort)
        assert_equals(str(tan), result.tan)
        assert_equals(str(request_pharmacy_id), result.apoIK)
        assert_length(1, RecordedRequest.query.all())

    def test_returns_correct_error_message_for_unknown_tan(self):
        tan_string = str(987654321)
        pharmacy_1 = Pharmacy.example(pharmacy_id=111111111, name='A')
        pharmacy_2 = Pharmacy.example(pharmacy_id=222222222, name='B')
        Prescription.example(pharmacy=pharmacy_1, transaction_number=123456786)
        p2 = Prescription.example(pharmacy=pharmacy_2, transaction_number=tan_string)
        assert_equals(p2.transaction_number, tan_string,
            message='Ensure there is always a filter on pharmacy_id.')
        satz_data = dict(
            tan=tan_string,
            apoIK=str(pharmacy_1.pharmacy_id),
            abgabedatum='2014-10-02',
            recordID=123456
        )
        error = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(error, FehlerSatz)
        assert_equals(str(pharmacy_1.pharmacy_id), error.apoIK)
        assert_equals(9100, error.fehlercode,
            message='TAN was not identified as "unknown"')
        assert_equals('TAN nicht gefunden', error.parameter)
        assert_length(0, RecordedRequest.query.all())

    def test_can_detect_if_we_were_not_in_charge_of_pharmacy_for_requested_period_if_tan_was_not_found(self):
        tan = 987654321
        pharmacy = Pharmacy.example(
            pharmacy_id=111111111,
            name='A',
            periods=(
                PharmacyPeriod(since=Date(2010, 1, 1), until=Date(2011, 12, 31)),
            ),
        )
        satz_data = dict(
            tan=str(tan),
            apoIK=str(pharmacy.pharmacy_id),
            abgabedatum='2014-10-02',
            recordID=123456
        )
        error = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(error, FehlerSatz)
        assert_equals(str(pharmacy.pharmacy_id), error.apoIK)
        assert_equals(9201, error.fehlercode) # Apotheke in diesem Zeitraum nicht gelistet
        assert_equals('Apotheke in diesem Abrechnungszeitraum unbekannt', error.parameter)
        assert_length(0, RecordedRequest.query.all())

        # verify that TAN is the primary lookup key and information about
        # dates is only used if no such TAN is found
        Prescription.example(pharmacy=pharmacy, transaction_number=tan)
        result = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(result, ParenteraliaSatzAntwort)
        assert_length(1, RecordedRequest.query.all())

    def test_returns_prescription_with_closest_delivery_date_if_tan_is_not_unique(self):
        tan = 987654321
        pharmacy = Pharmacy.example(pharmacy_id=111111111, name='A')
        requested_date = Date(2012, 1, 1)
        p1_date = requested_date + TimeDelta(days=2)
        p2_date = requested_date - TimeDelta(days=1)
        assert_smaller(p2_date.year, requested_date.year,
            message='Test that even year-wrapping edge cases just check the number of days')
        p1 = Prescription.example(pharmacy=pharmacy, transaction_number=tan, delivery_date=p1_date)
        p2 = Prescription.example(pharmacy=pharmacy, transaction_number=tan, delivery_date=p2_date)
        satz_data = dict(
            tan=str(tan),
            apoIK=str(pharmacy.pharmacy_id),
            abgabedatum=xsd.Date().xmlvalue(requested_date),
            recordID=123456,
        )
        result = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(result, ParenteraliaSatzAntwort)
        assert_equals(p2.delivery_date, result.abgabedatum.as_datetime_date())
        assert_length(1, RecordedRequest.query.all())

        satz_data['recordID'] = 654321
        satz_data['abgabedatum'] = p1_date - TimeDelta(days=1)
        result = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(result, ParenteraliaSatzAntwort)
        assert_equals(p1.delivery_date, result.abgabedatum.as_datetime_date())
        assert_length(2, RecordedRequest.query.all())


    def test_handles_duplicate_record_id_for_same_prescription_gracefully(self):
        # This test covers a special case that ZESAR sends us the same TAN
        # with a previously sent record id. This might happen due to connection
        # errors where the ZESAR platform tries to resend the data even though
        # we assumed they got our response. (Read the code for a more detailed
        # analysis).
        logger = logging.getLogger('srw.zesar.soap.sendeParenteraliadaten.method')
        logger.setLevel(logging.FATAL)
        pharmacy = Pharmacy.example(
            pharmacy_id=112233445,
            name='Löwenapotheke',
            street='Eichendorffstr. 71',
            postal_code='12345',
            city='Holzhausen',
            country_code='de',
        )
        Prescription.example(
            pharmacy=pharmacy,
            transaction_number=123456786,
            year=2014,
            delivery_date=Date(2014, 10, 2),
            special_code=1654987,
            valuation=10031,
            currency='EUR',
            data={'positions': ({'pzn': 9999042, 'faktor': 100},)},
        )
        assert_length(0, RecordedRequest.query.all())
        satz_data = dict(
            tan='123456786',
            apoIK=str(pharmacy.pharmacy_id),
            abgabedatum='2014-10-02',
            recordID=654321
        )
        result = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(result, ParenteraliaSatzAntwort)
        assert_equals('123456786', result.tan)
        assert_equals(654321, result.recordID)

        # Now we're sending the request again...
        result = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(result, ParenteraliaSatzAntwort)
        assert_equals('123456786', result.tan)
        assert_equals(654321, result.recordID)
        # .one() ensure that we recorded only a single request!
        recorded_request = RecordedRequest.query.one()
        assert_equals(654321, recorded_request.record_id)
        assert_equals(123456786, recorded_request.transaction_number)

    def test_hides_components_where_factor_equals_zero(self):
        # ZESAR GmbH requested this, might happen for some customers due to the
        # way they deliver their data.
        tan = 987654321
        pzn = 234567
        delivery_date = Date(2014, 10, 2)
        pharmacy = Pharmacy.example(pharmacy_id=111111111, name='A')
        Prescription.example(pharmacy=pharmacy, transaction_number=tan, delivery_date=delivery_date,
            data={'positions': ({'pzn': pzn, 'faktor': 4}, {'pzn': '12345678', 'faktor': 0})})
        satz_data = dict(
            tan=str(tan),
            apoIK=str(pharmacy.pharmacy_id),
            abgabedatum=xsd.Date().xmlvalue(delivery_date),
            recordID=123456,
        )

        result = self._request_parenteralia([satz_data], self.user)
        assert_isinstance(result, ParenteraliaSatzAntwort)
        assert_equals('%09d' % tan, result.tan)
        assert_length(1, result.einzelbestandteile,
            message='Expected only one as components with factor = 0 should be hidden.')
        data = result.einzelbestandteile[0]
        # we don't have enough knowledge in the ZESAR server to decide if PZNs
        # should have 7 or 8 digits, so the server will pad to 7
        assert_equals('%07d' % pzn, data.pzn)
        assert_equals(4, data.faktor)

    # --- internal helpers ----------------------------------------------------
    def _assemble_soap_body(self, items=()):
        xml_items = []
        for item in items:
            item_xml = \
                '''<ns3:parenteraliaSatz>
                        <ns3:tan>%(tan)s</ns3:tan>
                        <ns3:abgabedatum>%(abgabedatum)s</ns3:abgabedatum>
                        <ns3:apoIK>%(apoIK)s</ns3:apoIK>
                        <ns3:recordID>%(recordID)s</ns3:recordID>
                    </ns3:parenteraliaSatz>''' % item
            xml_items.append(item_xml)
        return \
         '''<ns3:parenteraliaAnfrage xmlns="http://zesargmbh.com/zesar/schema/akzeptRechnungsdatenLieferung/v1_0_0"
                xmlns:ns3="http://zesargmbh.com/zesar/schema/parenteraliaAnfrage/v1_0_0">
                    %s
                </ns3:parenteraliaAnfrage>''' % ('\n'.join(xml_items))

    def _soap_request(self, body_xml, user):
        soap_xml = (
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
                '<soapenv:Body>' + \
                    body_xml + \
                '</soapenv:Body>'
            '</soapenv:Envelope>'
        )
        environ = {'repoze.who.identity': {'login': user.username}}
        return SOAPRequest(environ, soap_xml)

    def _validated_input(self, request_xml):
        element = self.schema.elements[self.method.input]
        input_parser = element._type
        return input_parser.parsexml(request_xml, schema=self.schema)

    def _process_request(self, request):
        SOAP = ZesarService.version
        envelope = SOAP.Envelope.parsexml(request.http_content)
        body_xml = etree.tostring(envelope.Body.content())
        input_ = self._validated_input(body_xml)
        return self.method.function(request, input_)

    def _request_parenteralia(self, request_items, user):
        body_xml = self._assemble_soap_body(request_items)
        request = self._soap_request(body_xml, user)
        response = self._process_request(request)
        assert_isinstance(response, ParenteraliaAntwort)
        assert_length(len(request_items), response.parenteralia_antworten,
            message='Expected one response per request item.')
        if len(response.parenteralia_antworten) == 1:
            # makes calling test code a tiny bit more readable
            return response.parenteralia_antworten[0]
        return response.parenteralia_antworten

