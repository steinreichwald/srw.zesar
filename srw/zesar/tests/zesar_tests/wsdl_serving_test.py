# encoding: utf-8

from __future__ import absolute_import

from lxml import etree
from pyramid.testing import DummyRequest
from pyramid import testing
from pythonic_testcase import *

from srw.zesar.routes_ import add_routes
from srw.zesar.testutil import FunctionalTestCase
from srw.zesar.zesar_views import serve_wsdl, serve_xsd


# admittedly the WSDLServingTest is a bit superfluous as it basically just
# checks the same stuff as the WSDLServingFunctionalTest. However it is also
# intended as a pattern for other (SOAP) tests where the functional test just
# checks the happy case.
class WSDLServingTest(PythonicTestCase):
    def setUp(self):
        self.request = DummyRequest(application_url='https://srw.example/foo/bar')
        self.config = testing.setUp(request=self.request)
        add_routes(self.config)

    def tearDown(self):
        testing.tearDown()

    def test_can_serve_wsdl_request(self):
        self.request.params['wsdl'] = ''
        response = serve_wsdl(self.request)
        assert_equals(200, response.status_code)
        wsdl_content = response.body
        wsdl_root = etree.fromstring(wsdl_content)
        assert_equals('{http://schemas.xmlsoap.org/wsdl/}definitions', wsdl_root.tag)
        wsdl_string = wsdl_content.decode('utf-8')
        ws_url = 'https://srw.example/foo/bar'
        assert_contains('<soap:address location="%s/"/>' % ws_url, wsdl_string)
        assert_contains(
            'schemaLocation="%s/static/xsd/Schema_parenteraliaAntwort_V1.0.0.xsd"/>' % ws_url,
            wsdl_string
        )

    def test_can_serve_xsd_files(self):
        self.request.matchdict['name'] = 'Schema_parenteraliaAnfrage_V1.0.0'
        response = serve_xsd(self.request)
        assert_equals(200, response.status_code)
        xsd_string = response.body
        xsd_root = etree.fromstring(xsd_string)
        assert_equals('{http://www.w3.org/2001/XMLSchema}schema', xsd_root.tag)

    def test_can_return_404_for_unknown_xsd_files(self):
        self.request.matchdict['name'] = 'invalid_schema'
        response = serve_xsd(self.request)
        assert_equals(404, response.status_code)


class WSDLServingFunctionalTest(FunctionalTestCase):
    def test_can_serve_xsd_files(self):
        response = self.app.get('/static/xsd/Schema_parenteraliaAnfrage_V1.0.0.xsd')
        assert_equals(200, response.status_int)
        assert_contains('<xs:schema', response)

    def test_can_reject_invalid_xsd_urls(self):
        response = self.app.get('/static/xsd/invalid.xsd', expect_errors=True)
        assert_equals(404, response.status_int)

    def test_can_serve_wsdl(self):
        response = self.app.get('?wsdl')
        assert_equals(200, response.status_int)
        wsdl_string = response.unicode_body
        assert_contains('<wsdl:definitions', wsdl_string)
        ws_url = 'https://srw.example/foo/bar'
        assert_contains('<soap:address location="%s/"/>' % ws_url, wsdl_string)
        assert_contains(
            'schemaLocation="%s/static/xsd/Schema_parenteraliaAntwort_V1.0.0.xsd"/>' % ws_url,
            wsdl_string
        )
