# encoding: utf-8

from __future__ import absolute_import

from datetime import datetime as DateTime, timedelta as TimeDelta

from babel.util import LOCALTZ
from lxml import etree
from pythonic_testcase import *
from soapfish.core import SOAPRequest

from srw.zesar.models import AcceptedFee, User
from srw.zesar.soap.ladeAkzeptRechnungsdaten import (
    ladeAkzeptRechnungsdaten_method,
    AkzeptRechnungsdatenLieferungSchema, AkzeptRechnungsdatenQuittung,
)
from srw.zesar.soap.service import ZesarService
from srw.zesar.testutil import DBTestCase


class LadeAkzeptRechnungsdatenTest(DBTestCase):
    def setUp(self):
        super(LadeAkzeptRechnungsdatenTest, self).setUp()
        self.method = ladeAkzeptRechnungsdaten_method
        self.schema = AkzeptRechnungsdatenLieferungSchema
        self.user = User.example()

    def test_can_store_accepted_fees(self):
        assert_length(0, AcceptedFee.query.all())
        rech_position = dict(
            tan='987654321',
            apoIK='112233445',
            recordID=155494,
            forderungApo=200,
            forderungArz=100,
            akzeptiertApo=50,
            akzeptiertArz=75,
            minGruende=(123,),
        )
        fees_response = self._send_accepted_fees([rech_position], self.user)
        assert_equals(1, fees_response.anzDs)
        assert_equals(50, fees_response.summeApo)
        assert_equals(75, fees_response.summeArz)

        fee = AcceptedFee.query.one()
        assert_equals(987654321, fee.transaction_number)
        assert_equals(112233445, fee.pharmacy_id)
        assert_equals(155494, fee.record_id)
        assert_equals(200, fee.requested_fee_pharmacy)
        assert_equals(100, fee.requested_fee_datacenter)
        assert_equals(50, fee.accepted_fee_pharmacy)
        assert_equals(75, fee.accepted_fee_datacenter)
        assert_equals((123,), fee.reduction_codes())
        assert_almost_equals(
            DateTime.now(tz=LOCALTZ),
            fee.submission_time,
            max_delta=TimeDelta(seconds=2)
        )

    # --- internal helpers ----------------------------------------------------
    def _assemble_soap_body(self, positions):
        positions_xml = ''
        for position in positions:
            gruende_xml = ''
            for grund in position.get('minGruende', ()):
                gruende_xml += '<minGrund>%d</minGrund>' % grund
            position['minGruende'] = gruende_xml
            xml = '''
            <akzeptRechPosition>
                <tan>%(tan)s</tan>
                <apoIK>%(apoIK)s</apoIK>
                <recordID>%(recordID)d</recordID>
                <forderungApo>%(forderungApo)d</forderungApo>
                <forderungArz>%(forderungArz)d</forderungArz>
                <akzeptiertApo>%(akzeptiertApo)d</akzeptiertApo>
                <akzeptiertArz>%(akzeptiertArz)d</akzeptiertArz>
                %(minGruende)s
            </akzeptRechPosition>
            ''' % position
            positions_xml += xml
        return \
         '''<akzeptRechnungsdatenLieferung xmlns="http://zesargmbh.com/zesar/schema/akzeptRechnungsdatenLieferung/v1_0_0">
                %s
            </akzeptRechnungsdatenLieferung>''' % positions_xml

    def _soap_request(self, body_xml, user):
        soap_xml = (
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
                '<soapenv:Body>' + \
                    body_xml + \
                '</soapenv:Body>'
            '</soapenv:Envelope>'
        )
        environ = {'repoze.who.identity': {'login': user.username}}
        return SOAPRequest(environ, soap_xml)

    def _validated_input(self, request_xml):
        element = self.schema.elements[self.method.input]
        input_parser = element._type
        return input_parser.parsexml(request_xml, schema=self.schema)

    def _process_request(self, request):
        SOAP = ZesarService.version
        envelope = SOAP.Envelope.parsexml(request.http_content)
        body_xml = etree.tostring(envelope.Body.content())
        input_ = self._validated_input(body_xml)
        return self.method.function(request, input_)

    def _send_accepted_fees(self, positions, user):
        body_xml = self._assemble_soap_body(positions)
        request = self._soap_request(body_xml, user)
        response = self._process_request(request)
        assert_isinstance(response, AkzeptRechnungsdatenQuittung)
        return response

