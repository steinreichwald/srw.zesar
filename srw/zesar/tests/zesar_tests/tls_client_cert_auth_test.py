# encoding: utf-8

from __future__ import absolute_import, unicode_literals

from pythonic_testcase import *
from soapfish import soap11 as SOAP

from srw.zesar.testutil import zesar_demo_cert, zesar_demo_md5, FunctionalTestCase
from srw.zesar.models import User


class TLSClientCertAuthTest(FunctionalTestCase):
    def test_rejects_soap_requests_if_no_tls_certificate_was_presented(self):
        headers = {'X-SSL-CLIENT-CERT': ''}
        response = self.app.post('/', headers=headers, expect_errors=True)
        assert_equals(403, response.status_int)

    def test_rejects_soap_requests_if_client_presented_malformed_tls_certificate(self):
        headers = {
            'X-SSL-CLIENT-CERT': '-----BEGIN CERTIFICATE-----\nFOOBARBAZ\n-----END CERTIFICATE-----\n'
        }
        response = self.app.post('/', headers=headers, expect_errors=True)
        assert_equals(403, response.status_int)

    def test_rejects_soap_requests_if_client_presented_invalid_tls_certificate(self):
        headers = {'X-SSL-CLIENT-CERT': unknown_but_valid_tls_certificate}
        response = self.app.post('/', headers=headers, expect_errors=True)
        assert_equals(403, response.status_int)

    def test_accepts_soap_request_with_valid_tls_certificate(self):
        User.example(username='zesar', password_hash=zesar_demo_md5, is_zesar=True)
        headers = {'X-SSL-CLIENT-CERT': zesar_demo_cert}
        response = self.app.post('/', headers=headers, expect_errors=True)
        assert_equals(500, response.status_int)

        envelope = SOAP.Envelope.parsexml(response.unicode_body)
        assert_trueish(envelope.Body.Fault)
        code, message, actor = SOAP.parse_fault_message(envelope.Body.Fault)
        assert_equals(SOAP.Code.CLIENT, code)
        assert_contains('XMLSyntaxError', message)
        assert_none(actor)


unknown_but_valid_tls_certificate = \
"""-----BEGIN CERTIFICATE-----
MIIEBDCCAewCASowDQYJKoZIhvcNAQEFBQAwQjELMAkGA1UEBhMCWFgxFTATBgNV
BAcMDERlZmF1bHQgQ2l0eTEcMBoGA1UECgwTRGVmYXVsdCBDb21wYW55IEx0ZDAe
Fw0xNDEwMjQxNDU3MzRaFw0yNDEwMjExNDU3MzRaME4xCzAJBgNVBAYTAkRFMREw
DwYDVQQIDAhJcmdlbmR3bzEVMBMGA1UEBwwMRGVmYXVsdCBDaXR5MRUwEwYDVQQK
DAxaRVNBUiBjbGllbnQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDv
QKOr3NqBWbW+ms1YUI2Qvk0IiIf1ZEHSR6nNkzrv5LX81yW0gY6BvNRkIgLib85P
bNL3F8ToxOPAmjQjTI9IW15uLcn6EXNt3Kgxs0QyX4WSLeU9GdKRwF1MKn2kLZ1Y
zF9HG7Rtv4rYJkBMIRqfi1ok/LFvEPhs3S/iiuhQAFe2sN3Ef8qrIiCMrNAqufmh
WAs6e4zq3dNpSzU4I3qKlyxIC0HUmtdb9qNraKgKkZMzakszwt4PSXZnd1fIlQMo
POBLw1PJm32n7tlC6HhelZzxMUR7F0GZTWfA4Y/1cJPoPqMZMHXw0SHqU1dC/RQy
uaPq3xq1EMT96jj5ho1HAgMBAAEwDQYJKoZIhvcNAQEFBQADggIBAA2w5sriDL80
vSiygFSrtOAXUc8REKfOm8XO5tmrTwrxCxTKszNpdli1ugTETyyqR4923KLYklWl
ibEJrJdwDhm+5Yk0hS2QW0+HRutYp4VPqew29OydYOas+/6Qd5+cfi/8BB2vLr9G
09kwTPpCyhDLSrfT3p3arQUFv/0aGp8WXbGDvNcMAu/e/QHClxdIj8QQt/c+TkKt
JHENtuBPfwPeXaa3G+E8jV71UJTN4P9Ob35CE3fetk9l/THw4HfvHDJp75uKIs0p
ZXpGMg1QvNr+aIwIHArL/6qhDzdrT4lMHPrVRlycwioC8lBv3gjVFINvRkwW1rXl
pHmLuxZVdXvrU2aKMJboQpaci/bjo0xOB+Lka81ahWHvN+nJSayh0PT4blyBxtJy
S5f3JhQr+J9v2T+ANUiDcFXnqnDf+nNP18gwm3UNxb+yNpWSZ3Tx8FspFf278qlD
fMNfx3c0Lcf90Ynt57sn2t3CzpwYok4P2ERJPxMnGESL3sooSlak8oAuV6O8UXC0
dKPTFjAWBaoeOtjhIdIy/8f1d8mpyvL9bRKaeUjODEK/OnCHK29P8RyP23wOopiV
FObyAu2r2YKTHQ+co2eVLAzscRV4eYxw1GB8Vb0F+GW3/bOWceN1bzDPE49q/saq
p/LJwIZkxL2HnqZWUx9reoO7wLjtDl8T
-----END CERTIFICATE-----
"""
