# encoding: utf-8

from __future__ import absolute_import

from datetime import date as Date, datetime as DateTime

from babel.util import UTC
from ddt import ddt as DataDrivenTestCase, data
from lxml import etree
from nose.tools import assert_not_is_instance
from pythonic_testcase import *
from soapfish.core import SOAPRequest, SOAPError

from srw.zesar.models import Pharmacy, Prescription, RecordedRequest, User
from srw.zesar.testutil import DBTestCase
from srw.zesar.soap.sendeRechnungsdaten import (sendeRechnungsdaten_method,
    RechnungsdatenAnfrageSchema, RechnungsdatenAntwort
)
from srw.zesar.soap.service import ZesarService


@DataDrivenTestCase
class sendeRechnungsdatenTest(DBTestCase):
    def setUp(self):
        super(sendeRechnungsdatenTest, self).setUp()
        self.method = sendeRechnungsdaten_method
        self.schema = RechnungsdatenAnfrageSchema
        self.user = User.example()

    @data((2014, 50), (2010, 0))
    def test_can_return_error_for_invalid_months(self, year_month):
        body_xml = self._assemble_soap_body(*year_month)
        request = self._soap_request(body_xml, self.user)
        fault = self._process_request(request)
        assert_isinstance(fault, SOAPError)
        SOAP = ZesarService.version
        assert_equals(SOAP.Code.CLIENT, fault.code)
        yearmonth = '%04d%02d' % year_month
        assert_equals('Invalid invoicing period %r specified.' % yearmonth, fault.message)

    def test_returns_empty_output_if_no_requests_were_recorded(self):
        assert_length(0, RecordedRequest.query.all())
        positions = self._request_invoice(2010, 10, self.user)
        assert_length(0, positions)

    def test_can_return_invoice_data(self):
        pharmacy = Pharmacy.example(pharmacy_id=112233445)
        request1 = RecordedRequest.example(
            record_id=555666777,
            pharmacy=pharmacy,
            transaction_number=987654321,
            # Europe/Berlin is UTC+1 for that time
            request_time=DateTime(2014, 10, 31, 22, 59, 59, tzinfo=UTC),
            user=self.user,
        )
        request2 = RecordedRequest.example(
            record_id=1222333444,
            pharmacy=pharmacy,
            transaction_number=123456789,
            request_time=DateTime(2014, 11, 5, tzinfo=UTC),
            user=self.user,
        )
        assert_length(2, RecordedRequest.query.all())

        position = self._request_invoice(2014, 10, self.user)
        assert_not_is_instance(position, tuple)
        assert_equals('%09d' % request1.transaction_number, position.tan)
        assert_equals(str(pharmacy.pharmacy_id), position.apoIK)
        assert_equals(request1.record_id, position.recordID)
        assert_equals(200, position.forderungApo)
        assert_equals(100, position.forderungArz)

        position = self._request_invoice(2014, 11, self.user)
        assert_equals('%09d' % request2.transaction_number, position.tan)
        assert_equals(str(pharmacy.pharmacy_id), position.apoIK)
        assert_equals(request2.record_id, position.recordID)
        assert_equals(200, position.forderungApo)
        assert_equals(100, position.forderungArz)

    def test_demands_increased_compensation_for_deliveries_in_june_2011(self):
        # as specified in the ZESAR API 1.0 specs (2012-08-21)
        #    3.2.3.2 Ausgabedaten (Rechnungsdaten), page 20/21
        pharmacy = Pharmacy.example(pharmacy_id=112233445)
        tan = 987654321
        prescription = Prescription.example(
            pharmacy=pharmacy,
            transaction_number=tan,
            year=2011,
            delivery_date=Date(2011, 6, 2),
            special_code=1654987,
            valuation=10031,
            currency='EUR',
            data={'positions': ({'pzn': 9999042, 'faktor': 100},)},
        )
        request = RecordedRequest.example(
            record_id=555666777,
            pharmacy=pharmacy,
            prescription_id=prescription.id,
            transaction_number=tan,
            # request time does not matter for the increased compensation
            request_time=DateTime(2011, 6, 2, 10, 59, 59, tzinfo=UTC),
            user=self.user,
        )
        assert_length(1, RecordedRequest.query.all())

        position = self._request_invoice(2011, 6, self.user)
        assert_equals('%09d' % request.transaction_number, position.tan)
        assert_equals(str(pharmacy.pharmacy_id), position.apoIK)
        assert_equals(request.record_id, position.recordID)
        assert_equals(300, position.forderungApo)
        assert_equals(100, position.forderungArz)


    def test_can_add_leading_zeros_if_required(self):
        pharmacy_id = 11223344
        assert_length(8, str(pharmacy_id),
            message='IK numbers must have 9 digits, having only 8 requires adding leading zeros')
        transaction_number = 98765432
        assert_length(8, str(transaction_number),
            message='transaction numbers must have 9 digits, having only 8 requires adding leading zeros')
        pharmacy = Pharmacy.example(pharmacy_id=pharmacy_id)
        request = RecordedRequest.example(
            record_id=555666777,
            pharmacy=pharmacy,
            transaction_number=transaction_number,
            # Europe/Berlin is UTC+1 for that time
            request_time=DateTime(2014, 10, 31, 22, 59, 59, tzinfo=UTC),
            user=self.user,
        )

        with assert_not_raises():
            position = self._request_invoice(2014, 10, self.user)
        assert_not_is_instance(position, tuple)
        assert_equals('0'+str(request.transaction_number), position.tan)
        assert_equals('0'+str(pharmacy.pharmacy_id), position.apoIK)
        assert_equals(request.record_id, position.recordID)

    # --- internal helpers ----------------------------------------------------
    def _assemble_soap_body(self, year, month):
        return \
         '''<ns5:rechnungsdatenAnfrage xmlns="http://zesargmbh.com/zesar/schema/akzeptRechnungsdatenLieferung/v1_0_0"
                xmlns:ns5="http://zesargmbh.com/zesar/schema/rechnungsdatenAnfrage/v1_0_0">
                    <ns5:abrechnungsmonat>%(year)04d%(month)02d</ns5:abrechnungsmonat>
            </ns5:rechnungsdatenAnfrage>''' % dict(year=year, month=month)

    def _soap_request(self, body_xml, user):
        soap_xml = (
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
                '<soapenv:Body>' + \
                    body_xml + \
                '</soapenv:Body>'
            '</soapenv:Envelope>'
        )
        environ = {'repoze.who.identity': {'login': user.username}}
        return SOAPRequest(environ, soap_xml)

    def _validated_input(self, request_xml):
        element = self.schema.elements[self.method.input]
        input_parser = element._type
        return input_parser.parsexml(request_xml, schema=self.schema)

    def _process_request(self, request):
        SOAP = ZesarService.version
        envelope = SOAP.Envelope.parsexml(request.http_content)
        body_xml = etree.tostring(envelope.Body.content())
        input_ = self._validated_input(body_xml)
        return self.method.function(request, input_)

    def _request_invoice(self, year, month, user):
        body_xml = self._assemble_soap_body(year, month)
        request = self._soap_request(body_xml, user)
        response = self._process_request(request)
        assert_isinstance(response, RechnungsdatenAntwort)
        if len(response.positionen) == 1:
            # makes calling test code a tiny bit more readable
            return response.positionen[0]
        return response.positionen

