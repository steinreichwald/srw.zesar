# encoding: utf-8

from __future__ import absolute_import

from datetime import date as Date, datetime as DateTime

from babel.util import LOCALTZ, UTC
from pythonic_testcase import *
from soapfish.py2xsd import schema_validator

from srw.zesar.models import Pharmacy, Prescription, RecordedRequest, User
from srw.zesar.soap.ladeAkzeptRechnungsdaten import (
    ladeAkzeptRechnungsdaten_method, AkzeptRechnungsdatenLieferung,
    AkzeptRechnungsdatenQuittung, AkzeptRechPosition)
from srw.zesar.soap.sendeParenteraliadaten import (
    sendeParenteraliadaten_method, ParenteraliaAnfrage,
    ParenteraliaAntwort, ParenteraliaSatzAnfrage)
from srw.zesar.soap.sendeRechnungsdaten import (sendeRechnungsdaten_method,
    RechnungsdatenAnfrage, RechnungsdatenAntwort)
from srw.zesar.soap.service import ZesarService
from srw.zesar.testutil import zesar_demo_cert, zesar_demo_md5, FunctionalTestCase


class ZESARFunctionalTest(FunctionalTestCase):
    def setUp(self):
        super(ZESARFunctionalTest, self).setUp()
        self.user = User.example(username='zesar', password_hash=zesar_demo_md5, is_zesar=True)

    def test_can_return_parenteralia_data_and_record_requests_for_billing(self):
        pharmacy = Pharmacy.example(pharmacy_id=112233445, name='Löwenapotheke')
        prescription = Prescription.example(
            pharmacy=pharmacy,
            transaction_number=123456786,
            delivery_date=Date(2014, 10, 2),
            data={'positions': ({'pzn': 9999042, 'faktor': 100},)},
        )
        tan_string = str(prescription.transaction_number)
        parenteralia_request = ParenteraliaSatzAnfrage.create(
            tan=tan_string,
            abgabedatum=prescription.delivery_date,
            apoIK=str(pharmacy.pharmacy_id),
            recordID=654321
        )
        parameters = ParenteraliaAnfrage.create(parenteralia_saetze=[parenteralia_request])
        method = sendeParenteraliadaten_method
        response = self._soap_request(method, parameters)
        parenteralia_response = self._parse_response(response, ParenteraliaAntwort)

        assert_length(1, parenteralia_response.parenteralia_antworten)
        aw = parenteralia_response.parenteralia_antworten[0]
        assert_length(1, aw.einzelbestandteile)
        position = aw.einzelbestandteile[0]
        assert_equals('9999042', position.pzn)
        assert_equals(100, position.faktor)

        now = DateTime.now(tz=LOCALTZ)
        abrechnungsmonat = '%04d%02d' % (now.year, now.month)
        invoice_request = RechnungsdatenAnfrage.create(abrechnungsmonat=abrechnungsmonat)
        response = self._soap_request(sendeRechnungsdaten_method, invoice_request)
        invoice_response = self._parse_response(response, RechnungsdatenAntwort)
        assert_length(1, invoice_response.positionen)
        position = invoice_response.positionen[0]
        assert_equals(654321, position.recordID)
        assert_equals(tan_string, position.tan)

    def test_can_return_error_response_for_unknown_pharmacy(self):
        # Innovas testing uncovered a flaw that all response items used a static
        # xml tag name (parenteraliaSatz) even for error responses.
        parenteralia_request = ParenteraliaSatzAnfrage.create(
            tan=str(123456786),
            abgabedatum=Date(2014, 10, 2),
            apoIK=str(112233445),
            recordID=654321
        )
        parameters = ParenteraliaAnfrage.create(parenteralia_saetze=[parenteralia_request])
        method = sendeParenteraliadaten_method
        response = self._soap_request(method, parameters)
        parenteralia_response = self._parse_response(response, ParenteraliaAntwort)
        response_xml = response.unicode_body
        assert_not_contains(':parenteraliaSatz>', response_xml,
            message='server should not return any parenteralia data as pharmacy is unknown')
        assert_contains(':fehlerSatz>', response_xml,
            message='errors should be used correctly')
        response_items = parenteralia_response.parenteralia_antworten
        assert_length(1, response_items)
        error = response_items[0]
        assert_equals(9200, error.fehlercode)

    def test_can_send_invoice_data(self):
        pharmacy = Pharmacy.example(pharmacy_id=112233445, name='Löwenapotheke')
        recorded = RecordedRequest.example(
            record_id=555666777,
            pharmacy=pharmacy,
            transaction_number=987654321,
            # Europe/Berlin is UTC+1 for that time
            request_time=DateTime(2014, 11, 30, 22, 59, 59, tzinfo=UTC),
            user=self.user,
        )

        invoice_request = RechnungsdatenAnfrage.create(abrechnungsmonat='201411')
        method = sendeRechnungsdaten_method
        response = self._soap_request(method, invoice_request)
        invoice_response = self._parse_response(response, RechnungsdatenAntwort)
        assert_length(1, invoice_response.positionen)
        position = invoice_response.positionen[0]

        assert_equals(recorded.record_id, position.recordID)
        assert_equals(str(recorded.transaction_number), position.tan)
        assert_equals(str(recorded.pharmacy_id), position.apoIK)
        assert_equals(200, position.forderungApo)
        assert_equals(100, position.forderungArz)


    def test_can_store_accepted_request_fees(self):
        invoice_request = AkzeptRechnungsdatenLieferung.create(positionen=(
            AkzeptRechPosition.create(
                tan='987654321',
                apoIK='112233445',
                recordID=123,
                forderungApo=200,
                forderungArz=100,
                akzeptiertApo=50,
                akzeptiertArz=75,
                minGruende=(123,)
            ),
        ))
        method = ladeAkzeptRechnungsdaten_method
        response = self._soap_request(method, invoice_request)
        fees_response = self._parse_response(response, AkzeptRechnungsdatenQuittung)
        assert_equals(1, fees_response.anzDs)
        assert_equals(50, fees_response.summeApo)
        assert_equals(75, fees_response.summeArz)

    # --- internal helpers ---------------------------------------------------
    def _soap_request(self, method, parameters):
        headers = {
            'X-SSL-CLIENT-CERT': zesar_demo_cert,
            'ACCEPT': 'application/soap+xml,multipart/related,text/*',
            'SOAPAction': '"%s"' % method.soapAction,
        }
        SOAP = ZesarService.version
        tagname = method.input
        envelope = SOAP.Envelope.response(tagname, parameters)
        return self.app.post('/', headers=headers, params=envelope)

    def _parse_response(self, response, response_object):
        assert_equals(200, response.status_int)

        SOAP = ZesarService.version
        envelope = SOAP.Envelope.parsexml(response.unicode_body)
        assert_none(envelope.Body.Fault)
        body_content = envelope.Body.content()
        # ensure the returned XML is valid according to the schema
        schema_validator(ZesarService.schemas)(envelope.Body.content())
        return response_object.parse_xmlelement(body_content)

