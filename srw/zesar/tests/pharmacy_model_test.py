# encoding: utf-8

from __future__ import absolute_import

from datetime import date as Date

from pythonic_testcase import *

from srw.zesar.models import Pharmacy, PharmacyPeriod
from srw.zesar.testutil import DBTestCase


class PharmacyTest(DBTestCase):
    def test_can_create_example_instance(self):
        pharmacy = Pharmacy.example(
            pharmacy_id=987654321,
            name='Königsapotheke',
            street='Eichendorffstr. 71',
            postal_code='72406',
            city='Biesingen',
            country_code='de',
            periods=(
                PharmacyPeriod(since=Date(2010, 1, 4), until=Date(2015, 10, 8)),
            ),
        )
        assert_isinstance(pharmacy, Pharmacy)
        assert_equals(987654321, pharmacy.pharmacy_id)
        assert_equals('Eichendorffstr. 71', pharmacy.street)
        assert_equals('72406', pharmacy.postal_code)
        assert_equals('Biesingen', pharmacy.city)
        assert_equals('de', pharmacy.country_code)
        assert_length(1, pharmacy.periods)
        period = pharmacy.periods[0]
        assert_equals(Date(2010, 1, 4), period.since)
        assert_equals(Date(2015, 10, 8), period.until)

    def test_can_return_info_for_given_date(self):
        pharmacy = Pharmacy.example(
            pharmacy_id=987654321,
            name='Königsapotheke',
            street='Eichendorffstr. 71',
            postal_code='72406',
            city='Biesingen',
            country_code='de',
            periods=(
                PharmacyPeriod(since=Date(2010, 4, 1), until=Date(2012, 10, 31)),
                PharmacyPeriod(since=Date(2014, 4, 1), until=None),
            )
        )
        assert_equals(pharmacy, pharmacy.info_for(Date(2011, 1, 1)))
        assert_equals(pharmacy, pharmacy.info_for(Date(2010, 4, 1)))
        assert_equals(pharmacy, pharmacy.info_for(Date(2012, 10, 31)))

        assert_equals(pharmacy, pharmacy.info_for(Date(2014, 4, 1)))
        assert_equals(pharmacy, pharmacy.info_for(Date(2040, 1, 1)))

        assert_none(pharmacy.info_for(Date(2010, 3, 31)))
        assert_none(pharmacy.info_for(Date(2013, 1, 1)))

