# encoding: utf-8

from __future__ import absolute_import

from hashlib import md5

from pythonic_testcase import *
from soapfish.core import SOAPError, SOAPRequest

from srw.zesar.admin.basic_auth import BasicAuthHeader, BasicAuth
from srw.zesar.admin.authenticate_admin import authenticate_admin
from srw.zesar.auth import current_username
from srw.zesar.models import User
from srw.zesar.testutil import DBTestCase


class CheckCredentialsTest(DBTestCase):
    # --- authentication ------------------------------------------------------
    def test_can_check__credentials(self):
        user = self.create_user(u'foobar', u'secret', is_admin=True)
        self.assert_error('Client', username=u'invalid', password=u'secret')
        self.assert_error('Client', username=u'foobar', password=u'invalid')
        self.assert_authentication(user, username=user.username, password=u'secret')

    def test_reject_non_admin_user(self):
        zesar = self.create_user(u'zesar', u'secret', is_admin=False, is_zesar=True)
        self.assert_error('Client', username=zesar.username, password=u'secret')

        user = self.create_user(u'user', u'secret', is_admin=False, is_zesar=False)
        self.assert_error('Client', username=user.username, password=u'secret')

    # --- internal helpers ----------------------------------------------------
    def create_user(self, username, password, is_admin=False, is_zesar=False):
        md5_hash = md5(password.encode('utf8')).hexdigest()
        return User.example(
            username=username,
            password_hash=md5_hash,
            is_admin=is_admin,
            is_zesar=is_zesar,
        )

    def _soap_request(self, credentials):
        environ = {
            'settings': {
                # During testing a simple/insecure md5 hashing is enough and does
                # not slow down the tests.
                'passlib.schemes': 'hex_md5',
            }
        }
        request = SOAPRequest(environ, 'body')
        if credentials:
            username, password = credentials
            basic_auth = BasicAuth.create(Name=username, Password=password)
            request.soap_header = BasicAuthHeader.create(BasicAuth=basic_auth)
        return request

    def assert_error(self, faultcode, username, password):
        request = self._soap_request((username, password))
        soap_error = self.call(request)
        assert_isinstance(soap_error, SOAPError)
        assert_equals(faultcode, soap_error.code)

    def assert_authentication(self, user, username, password):
        request = self._soap_request((username, password))
        soap_error = self.call(request)
        assert_false(isinstance(soap_error, SOAPError))
        assert_equals(user.username, current_username(request.environ))

    def call(self, request):
        def echo_handler(request, input_):
            return request
        closured_decorator = authenticate_admin
        decorated_method = closured_decorator(echo_handler)
        return decorated_method(request, None)

