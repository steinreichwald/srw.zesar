# encoding: utf-8

from __future__ import absolute_import

from datetime import datetime as DateTime
from hashlib import md5

from babel.util import UTC
from lxml import etree
from pythonic_testcase import *
from soapfish import xsd
from soapfish.core import SOAPRequest, SOAPError

from srw.zesar.admin.queryRecordedRequests import (queryRecordedRequests_method,
    RecordedRequestInfo, RecordedRequestLog)
from srw.zesar.admin.service import AdminSchema, AdminService
from srw.zesar.lib.period import Period
from srw.zesar.models import AcceptedFee, Pharmacy, User, RecordedRequest
from srw.zesar.testutil import DBTestCase


class queryRecordedRequestsTest(DBTestCase):
    def setUp(self):
        super(queryRecordedRequestsTest, self).setUp()
        self.method = queryRecordedRequests_method
        self.schema = AdminSchema
        md5_hash = md5(b'secret').hexdigest()
        self.user = User.example(username='admin', password_hash=md5_hash, is_admin=True)

    def test_can_query_requests(self):
        pharmacy = Pharmacy.example(pharmacy_id=112233445)
        first = RecordedRequest.example(
            record_id=127,
            pharmacy=pharmacy,
            transaction_number=111111111,
            request_time=DateTime(2015, 1, 5, tzinfo=UTC),
            user=self.user,
        )
        second = RecordedRequest.example(
            record_id=128,
            pharmacy=pharmacy,
            transaction_number=111111112,
            request_time=DateTime(2015, 1, 20, tzinfo=UTC),
            user=self.user,
        )
        second_accepted = AcceptedFee.example(
            record_id=second.record_id,
            transaction_number=second.transaction_number,
            pharmacy_id=second.pharmacy_id,
            requested_fee_pharmacy=400,
            requested_fee_datacenter=200,
            accepted_fee_pharmacy=300,
            accepted_fee_datacenter=250,
            data={'reduction_codes': (213, )},
            submission_time=DateTime(2015, 3, 2, tzinfo=UTC),
            submitting_user=self.user.username,
        )
        assert_length(2, RecordedRequest.query.all())

        period = Period(
            DateTime(2015, 10, 1, tzinfo=UTC),
            DateTime(2015, 10, 30, tzinfo=UTC),
        )
        result = self._query_recorded_requests(period, credentials=('admin', 'secret'))
        assert_length(0, result)

        period = Period(
            DateTime(2015, 1, 2, tzinfo=UTC),
            DateTime(2015, 1, 10, tzinfo=UTC),
        )
        result = self._query_recorded_requests(period, credentials=('admin', 'secret'))
        assert_isinstance(result, RecordedRequestInfo)
        assert_equals(str(first.transaction_number), result.tan)

        period = Period(
            DateTime(2014, 1, 1, tzinfo=UTC),
            DateTime(2015, 2, 1, tzinfo=UTC),
        )
        result = self._query_recorded_requests(period, credentials=('admin', 'secret'))
        assert_length(2, result)
        assert_equals(str(first.transaction_number), result[0].tan)
        second_result = result[1]
        assert_equals(str(second.transaction_number), second_result.tan)
        assert_equals(400, second_result.forderungApotheke)
        assert_equals(200, second_result.forderungARZ)
        assert_equals(300, second_result.akzeptiertApotheke)
        assert_equals(250, second_result.akzeptiertARZ)
        assert_equals(second_accepted.submission_time, second_result.zeitpunktAkzeptanz)
        assert_equals((213,), second_result.minderungsgruende)

    def test_rejects_request_with_bad_credentials(self):
        body_xml = self._assemble_soap_body(
            Period(
                start=DateTime(1999, 1, 1, tzinfo=UTC),
                end=DateTime(2020, 12, 31, tzinfo=UTC)
            )
        )
        request = self._soap_request(body_xml, credentials=('admin', 'invalid'))
        response = self._process_request(request)
        assert_isinstance(response, SOAPError)

    # --- internal helpers ----------------------------------------------------
    def _assemble_soap_body(self, period):
        start_xml = xsd.DateTime().xmlvalue(period.start)
        end_xml = xsd.DateTime().xmlvalue(period.end)
        return \
         '''<queryRequest xmlns="https://www.steinreichwald.net/zesar/1.0">
                <start>%s</start>
                <end>%s</end>
            </queryRequest>''' % (start_xml, end_xml)

    def _soap_request(self, body_xml, credentials=None):
        if credentials is None:
            soap_header = ''
        else:
            username, password = credentials
            soap_header = (
                '<soapenv:Header>'
                    '<auth:BasicAuth '
                        'xmlns:auth="http://soap-authentication.org/basic/2001/10/" '
                        'soapenv:mustUnderstand="1">'
                        '<Name>%s</Name>' % username + \
                        '<Password>%s</Password>' % password + \
                    '</auth:BasicAuth>'
                '</soapenv:Header>')
        soap_xml = (
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' + \
                soap_header + \
                '<soapenv:Body>' + \
                    body_xml + \
                '</soapenv:Body>'
            '</soapenv:Envelope>'
        )
        environ = {
            'settings': {
                # During testing a simple/insecure md5 hashing is enough and does
                # not slow down the tests.
                'passlib.schemes': 'hex_md5',
            }
        }
        request = SOAPRequest(environ, soap_xml)
        SOAP = AdminService.version
        envelope = SOAP.Envelope.parsexml(soap_xml)
        request.soap_header = envelope.Header.parse_as(AdminService.input_header)
        return request

    def _validated_input(self, request_xml):
        element = self.schema.elements[self.method.input]
        input_parser = element._type
        return input_parser.parsexml(request_xml, schema=self.schema)

    def _process_request(self, request):
        SOAP = AdminService.version
        envelope = SOAP.Envelope.parsexml(request.http_content)
        body_xml = etree.tostring(envelope.Body.content())
        input_ = self._validated_input(body_xml)
        return self.method.function(request, input_)

    def _query_recorded_requests(self, period, credentials=None):
        body_xml = self._assemble_soap_body(period)
        request = self._soap_request(body_xml, credentials=credentials)
        response = self._process_request(request)
        assert_isinstance(response, RecordedRequestLog)
        if len(response.recorded_requests) == 1:
            # makes calling test code a tiny bit more readable
            return response.recorded_requests[0]
        return response.recorded_requests

