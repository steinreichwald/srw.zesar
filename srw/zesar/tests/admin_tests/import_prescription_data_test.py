# encoding: utf-8

from __future__ import absolute_import

from datetime import date as Date
from hashlib import md5

from lxml import etree
from pythonic_testcase import *
from soapfish.core import SOAPRequest, SOAPError
from soapfish.lib.attribute_dict import AttrDict

from srw.zesar.models import Pharmacy, Prescription, User
from srw.zesar.testutil import DBTestCase
from srw.zesar.admin.importPrescriptionData import (importPrescriptionData_method,
    ImportedPrescription, ImportResponse)
from srw.zesar.admin.service import AdminSchema, AdminService


class importPrescriptionDataTest(DBTestCase):
    def setUp(self):
        super(importPrescriptionDataTest, self).setUp()
        self.method = importPrescriptionData_method
        self.schema = AdminSchema
        md5_hash = md5(b'secret').hexdigest()
        self.user = User.example(username='admin', password_hash=md5_hash, is_admin=True)

    def test_can_import_prescription_data(self):
        pharmacy = Pharmacy.example(pharmacy_id=112233445)
        assert_length(0, Prescription.query.all())
        tan = '123456789'
        delivery_date = Date(2014, 10, 1)
        prescription = self._prescription_data(pharmacy, tan=tan, delivery_date=delivery_date)
        confirmation = self._import_prescriptions([prescription], credentials=('admin', 'secret'))

        assert_isinstance(confirmation, ImportedPrescription)
        assert_equals(str(pharmacy.pharmacy_id), confirmation.apoIK)
        assert_equals(tan, confirmation.tan)
        assert_equals(delivery_date, confirmation.abgabedatum.as_datetime_date())

        db_prescription = Prescription.query.one()
        assert_equals(pharmacy, db_prescription.pharmacy)
        assert_equals(delivery_date.year, db_prescription.year)
        assert_equals(delivery_date, db_prescription.delivery_date)
        assert_equals(int(tan), db_prescription.transaction_number)

        assert_equals(12345678, db_prescription.special_code)
        assert_equals(10000, db_prescription.valuation)
        assert_equals('EUR', db_prescription.currency)
        assert_equals((('1234567', 100),), db_prescription.positions())

    def test_handles_duplicate_import_gracefully(self):
        pharmacy = Pharmacy.example(pharmacy_id=112233445)
        assert_length(0, Prescription.query.all())
        tan = '123456789'
        delivery_date = Date(2014, 10, 1)
        prescription = self._prescription_data(pharmacy, tan=tan, delivery_date=delivery_date)

        confirmation = self._import_prescriptions([prescription], credentials=('admin', 'secret'))
        assert_isinstance(confirmation, ImportedPrescription)
        db_id = confirmation.id

        confirmation = self._import_prescriptions([prescription], credentials=('admin', 'secret'))
        assert_equals(db_id, confirmation.id)

    def test_rejects_prescriptions_for_unknown_pharmacies(self):
        tan = '123456789'
        delivery_date = Date(2014, 10, 1)
        unknown_pharmacy = AttrDict(pharmacy_id=112233445)
        prescription = self._prescription_data(unknown_pharmacy, tan=tan, delivery_date=delivery_date)
        body_xml = self._assemble_soap_body([prescription])
        request = self._soap_request(body_xml, credentials=('admin', 'secret'))

        response = self._process_request(request)
        assert_isinstance(response, SOAPError)
        assert_length(0, Prescription.query.all())

    def test_rejects_request_with_bad_credentials(self):
        pharmacy = Pharmacy.example(pharmacy_id=112233445)
        assert_length(0, Prescription.query.all())
        prescription = self._prescription_data(pharmacy)
        body_xml = self._assemble_soap_body([prescription])
        request = self._soap_request(body_xml, credentials=('admin', 'invalid'))
        response = self._process_request(request)
        assert_isinstance(response, SOAPError)

    # --- internal helpers ----------------------------------------------------
    def _prescription_data(self, pharmacy, tan='123456789', delivery_date=Date(2014, 10, 1)):
        return dict(
            apoIK=pharmacy.pharmacy_id,
            tan=tan,
            abgabedatum=delivery_date,
            sonderkennzeichen='12345678',
            taxe=10000,
            items=[dict(pzn='1234567', faktor=100)],
        )

    def _assemble_soap_body(self, prescriptions=()):
        xml_items = []
        for prescription in prescriptions:
            xml_einzelbestandteile = []
            for item in prescription['items']:
                item_xml = '''
                    <einzelbestandteil>
                        <pzn>%(pzn)s</pzn>
                        <faktor>%(faktor)s</faktor>
                    </einzelbestandteil>''' % dict(item.items())
                xml_einzelbestandteile.append(item_xml)
            prescription['einzelbestandteile'] = '\n'.join(xml_einzelbestandteile)
            item_xml = \
                '''<parenteraliaSatz>
                        <apoIK>%(apoIK)s</apoIK>
                        <tan>%(tan)s</tan>
                        <abgabedatum>%(abgabedatum)s</abgabedatum>
                        <sonderkennzeichen>%(sonderkennzeichen)s</sonderkennzeichen>
                        <taxe>%(taxe)s</taxe>
                        <waehrungskennzeichen>EUR</waehrungskennzeichen>
                        %(einzelbestandteile)s
                   </parenteraliaSatz>''' % prescription
            xml_items.append(item_xml)
        return \
         '''<importRequest xmlns="https://www.steinreichwald.net/zesar/1.0">
                    %s
            </importRequest>''' % ('\n'.join(xml_items))

    def _soap_request(self, body_xml, credentials=None):
        if credentials is None:
            soap_header = ''
        else:
            username, password = credentials
            soap_header = (
                '<soapenv:Header>'
                    '<auth:BasicAuth '
                        'xmlns:auth="http://soap-authentication.org/basic/2001/10/" '
                        'soapenv:mustUnderstand="1">'
                        '<Name>%s</Name>' % username + \
                        '<Password>%s</Password>' % password + \
                    '</auth:BasicAuth>'
                '</soapenv:Header>')
        soap_xml = (
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' + \
                soap_header + \
                '<soapenv:Body>' + \
                    body_xml + \
                '</soapenv:Body>'
            '</soapenv:Envelope>'
        )
        environ = {
            'settings': {
                # During testing a simple/insecure md5 hashing is enough and does
                # not slow down the tests.
                'passlib.schemes': 'hex_md5',
            }
        }
        request = SOAPRequest(environ, soap_xml)
        SOAP = AdminService.version
        envelope = SOAP.Envelope.parsexml(soap_xml)
        request.soap_header = envelope.Header.parse_as(AdminService.input_header)
        return request

    def _validated_input(self, request_xml):
        element = self.schema.elements[self.method.input]
        input_parser = element._type
        return input_parser.parsexml(request_xml, schema=self.schema)

    def _process_request(self, request):
        SOAP = AdminService.version
        envelope = SOAP.Envelope.parsexml(request.http_content)
        body_xml = etree.tostring(envelope.Body.content())
        input_ = self._validated_input(body_xml)
        return self.method.function(request, input_)

    def _import_prescriptions(self, prescriptions, credentials=None):
        body_xml = self._assemble_soap_body(prescriptions)
        request = self._soap_request(body_xml, credentials=credentials)
        response = self._process_request(request)
        assert_isinstance(response, ImportResponse)
        if len(response.prescriptions) == 1:
            # makes calling test code a tiny bit more readable
            return response.prescriptions[0]
        return response.prescriptions

