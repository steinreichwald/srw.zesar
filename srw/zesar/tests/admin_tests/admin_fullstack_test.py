# encoding: utf-8

from __future__ import absolute_import

from datetime import date as Date, datetime as DateTime
from hashlib import md5

from babel.util import UTC
from pythonic_testcase import *

from srw.zesar.admin.basic_auth import BasicAuthHeader, BasicAuth
from srw.zesar.admin.importPrescriptionData import (importPrescriptionData_method,
    PrescribedItem, PrescriptionData, ImportRequest, ImportResponse)
from srw.zesar.admin.queryRecordedRequests import (queryRecordedRequests_method,
    RecordedRequestsQuery, RecordedRequestLog)
from srw.zesar.admin.service import AdminService
from srw.zesar.models import Pharmacy, Prescription, RecordedRequest, User
from srw.zesar.testutil import FunctionalTestCase


class AdminFunctionalTest(FunctionalTestCase):
    def setUp(self):
        super(AdminFunctionalTest, self).setUp()
        self.password = 'secret'
        md5_hash = md5(self.password.encode('ascii')).hexdigest()
        self.user = User.example(username='admin', password_hash=md5_hash, is_admin=True)

    def test_can_import_prescriptions(self):
        pharmacy = Pharmacy.example(pharmacy_id=112233445)
        tan = '123456789'
        delivery_date = Date(2014, 10, 1)
        pharmacy_id_string = str(pharmacy.pharmacy_id)
        prescription = PrescriptionData.create(
            apoIK=pharmacy_id_string,
            tan=tan,
            abgabedatum=delivery_date,
            sonderkennzeichen='12345678',
            taxe=10000,
            waehrungskennzeichen='EUR',
            items=[PrescribedItem.create(pzn='1234567', faktor=100)]
        )
        parameters = ImportRequest.create(prescriptions=[prescription])
        method = importPrescriptionData_method
        response = self._soap_request(method, parameters)
        import_response = self._parse_response(response, ImportResponse)

        assert_length(1, import_response.prescriptions)
        prescription = import_response.prescriptions[0]
        assert_not_none(prescription.id)
        assert_equals(pharmacy_id_string, prescription.apoIK)
        assert_equals(tan, prescription.tan)
        assert_equals(delivery_date, prescription.abgabedatum.as_datetime_date())

        assert_length(1, Prescription.query.all())


    def test_can_query_recorded_requests(self):
        pharmacy = Pharmacy.example(pharmacy_id=112233446)
        username = 'cesar'
        zesar_user = User.example(username=username, is_zesar=True)
        request = RecordedRequest.example(
            record_id=4711,
            pharmacy=pharmacy,
            transaction_number=987654321,
            prescription=None,
            request_time=DateTime(2015, 4, 1, 23, 59, 50, tzinfo=UTC),
            user=zesar_user)

        parameters = RecordedRequestsQuery.create(
            start=DateTime(2015, 4, 1, tzinfo=UTC), end=DateTime(2015, 4, 30, tzinfo=UTC))
        method = queryRecordedRequests_method
        response = self._soap_request(method, parameters)
        query_response = self._parse_response(response, RecordedRequestLog)

        assert_length(1, query_response.recorded_requests)
        recorded = query_response.recorded_requests[0]
        assert_equals('112233446', recorded.apoIK)
        assert_equals(str(request.transaction_number), recorded.tan)
        assert_equals(request.request_time, recorded.abrufzeit)
        assert_equals(username, recorded.username)


    # --- internal helpers ---------------------------------------------------
    def _soap_request(self, method, parameters):
        headers = {
            'ACCEPT': 'application/soap+xml,text/xml',
            'SOAPAction': '"%s"' % method.soapAction,
        }
        SOAP = AdminService.version
        tagname = method.input
        basic_auth = BasicAuth.create(Name=self.user.username, Password=self.password)
        soap_header = BasicAuthHeader.create(BasicAuth=basic_auth)
        envelope = SOAP.Envelope.response(tagname, parameters, header=soap_header)
        return self.app.post('/admin/', headers=headers, params=envelope)

    def _parse_response(self, response, response_object):
        assert_equals(200, response.status_int)

        SOAP = AdminService.version
        envelope = SOAP.Envelope.parsexml(response.unicode_body)
        assert_none(envelope.Body.Fault)
        body_content = envelope.Body.content()
        return response_object.parse_xmlelement(body_content)

