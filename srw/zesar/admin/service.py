# -*- coding: utf-8 -*-

from __future__ import absolute_import

from soapfish import soap, xsd

from .basic_auth import *
from .definitions import srw_namespace, ws_admin_location
from .importPrescriptionData import *
from .queryRecordedRequests import *


__all__ = ['AdminSchema', 'AdminService']

AdminSchema = xsd.Schema(
    targetNamespace=srw_namespace,
    elementFormDefault=xsd.ElementFormDefault.QUALIFIED,
    imports=(BasicAuthSchema, ),
    complexTypes=(
        ImportedPrescription,
        ImportRequest,
        ImportResponse,
        PrescribedItem,
        PrescriptionData,
        RecordedRequestInfo,
        RecordedRequestLog,
        RecordedRequestsQuery,
    ),
    elements={
        'importRequest': xsd.Element(ImportRequest),
        'importResponse': xsd.Element(ImportResponse),
        'queryRequest': xsd.Element(RecordedRequestsQuery),
        'queryResponse': xsd.Element(RecordedRequestLog),
    },
)

################################################################################
# SOAP Service

AdminService = soap.Service(
    name='AdminSoapPort',
    targetNamespace=srw_namespace,
    # also used to calculate the SOAPAction
    location=ws_admin_location,
    schemas=(AdminSchema,),
    version=soap.SOAPVersion.SOAP11,
    input_header=BasicAuthHeader,
    methods=[
        importPrescriptionData_method,
        queryRecordedRequests_method,
    ],
)
