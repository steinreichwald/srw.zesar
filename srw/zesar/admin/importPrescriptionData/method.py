# -*- coding: utf-8 -*-

from soapfish import xsd
from soapfish.core import SOAPError

from ...lib.listify import tuplify
from ...models import DBSession, Pharmacy, Prescription
from ..authenticate_admin import authenticate_admin
from .response_types import ImportedPrescription, ImportResponse


__all__ = ['importPrescriptionData_method']

class PrescriptionDataImporter(object):
    @authenticate_admin
    def handle(self, request, importRequest):
        unknown_pharmacy_ids = self._find_unknown_pharmacies(importRequest.prescriptions)
        if unknown_pharmacy_ids:
            from ..service import AdminService
            SOAP = AdminService.version
            pharmacy_id = unknown_pharmacy_ids[0]
            return SOAPError(SOAP.Code.CLIENT, 'Unbekannte ApoIK %s' % pharmacy_id)
        db_prescriptions = []
        for data in importRequest.prescriptions:
            prescription = self._import_prescription(data)
            db_prescriptions.append(prescription)
        DBSession.commit()
        return self._response(db_prescriptions)

    def _import_prescription(self, data):
        delivery_date = data.abgabedatum.as_datetime_date()
        positions = []
        for position in data.items:
            positions.append({'pzn': position.pzn, 'faktor': position.faktor})
        prescription = Prescription.get_or_add(
            pharmacy_id=int(data.apoIK),
            year=delivery_date.year,
            transaction_number=int(data.tan),
        )
        if prescription.id is None:
            prescription.delivery_date = delivery_date
            prescription.special_code = int(data.sonderkennzeichen)
            prescription.valuation = int(data.taxe)
            prescription.currency = 'EUR'
            prescription.data = {'positions': positions}
        return prescription

    def _response(self, prescriptions):
        items = []
        for prescription in prescriptions:
            item = ImportedPrescription.create(
                id=prescription.id,
                apoIK='%09d' % prescription.pharmacy_id,
                tan='%09d' % prescription.transaction_number,
                abgabedatum = prescription.delivery_date,
            )
            items.append(item)
        return ImportResponse.create(prescriptions=items)

    @tuplify
    def _find_unknown_pharmacies(self, prescriptions_to_import):
        known_ids = set()
        for data in prescriptions_to_import:
            pharmacy_id = int(data.apoIK)
            pharmacy = Pharmacy.query.filter(Pharmacy.pharmacy_id == pharmacy_id).first()
            if pharmacy is not None:
                known_ids.add(pharmacy.pharmacy_id)
            else:
                yield pharmacy_id

importPrescriptionData_method = xsd.Method(
    'importPrescriptionData', 'https://www.steinreichwald.net/zesar/admin/importPrescriptionData',
    function=PrescriptionDataImporter().handle,
    input='importRequest',
    output='importResponse',
)

