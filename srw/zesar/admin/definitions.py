# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals


from ..soap.definitions import xsd_baseurl, ws_location as ws_base_location

__all__ = ['srw_namespace', 'ws_admin_location', 'xsd_baseurl']

srw_namespace = 'https://www.steinreichwald.net/zesar/1.0'
# This value is also used to determine the SOAPAction so it is more than just
# the URL of the currently running SOAP instance.
ws_admin_location = ws_base_location + 'admin/'
