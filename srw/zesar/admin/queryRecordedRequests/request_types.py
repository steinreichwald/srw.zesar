# -*- coding: utf-8 -*-

from soapfish import xsd

from ...soap.common_types import create_


__all__ = ['RecordedRequestsQuery']

class RecordedRequestsQuery(xsd.ComplexType):
    start = xsd.Element(xsd.DateTime)
    end = xsd.Element(xsd.DateTime)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)

