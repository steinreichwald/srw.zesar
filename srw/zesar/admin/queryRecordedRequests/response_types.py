# -*- coding: utf-8 -*-

from soapfish import xsd

from ...soap.common_types import create_


__all__ = ['RecordedRequestInfo', 'RecordedRequestLog']

class RecordedRequestInfo(xsd.ComplexType):
    recordID = xsd.Element(xsd.Integer)
    apoIK = xsd.Element(xsd.String(pattern='[0-9]{9}'))
    tan = xsd.Element(xsd.String(pattern='[0-9]{9}'))
    abrufzeit = xsd.Element(xsd.DateTime)
    username = xsd.Element(xsd.String)

    forderungApotheke = xsd.Element(xsd.Integer)
    forderungARZ = xsd.Element(xsd.Integer)
    akzeptiertApotheke = xsd.Element(xsd.Integer)
    akzeptiertARZ = xsd.Element(xsd.Integer)
    minderungsgruende = xsd.ListElement(
        xsd.Integer,
        'minderungsgrund',
        minOccurs=0,
        maxOccurs=xsd.UNBOUNDED
    )
    zeitpunktAkzeptanz = xsd.Element(xsd.DateTime)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


class RecordedRequestLog(xsd.ComplexType):
    recorded_requests = xsd.ListElement(
        RecordedRequestInfo,
        'recordedRequest',
        minOccurs=0,
        maxOccurs=xsd.UNBOUNDED
    )

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)

