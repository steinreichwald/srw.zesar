# -*- coding: utf-8 -*-

from sqlalchemy import and_
from soapfish import xsd

from ...models import RecordedRequest
from ..authenticate_admin import authenticate_admin
from .response_types import RecordedRequestInfo, RecordedRequestLog


__all__ = ['queryRecordedRequests_method']

class RecordedRequestsQuerier(object):
    @authenticate_admin
    def handle(self, request, queryRequest):
        start = queryRequest.start
        end = queryRequest.end
        query = RecordedRequest.query.filter(
            and_(
                RecordedRequest.request_time >= start,
                RecordedRequest.request_time <= end,
            )
        )
        return self._response(query.all())

    def _response(self, recorded_requests):
        items = []
        for recorded_request in recorded_requests:
            item = RecordedRequestInfo.create(
                apoIK='%09d' % recorded_request.pharmacy_id,
                tan='%09d' % recorded_request.transaction_number,
                abrufzeit = recorded_request.request_time,
                username = recorded_request.username,
            )
            if recorded_request.accepted_fees:
                fee = recorded_request.accepted_fees[0]
                item.forderungApotheke = fee.requested_fee_pharmacy
                item.forderungARZ = fee.requested_fee_datacenter
                item.akzeptiertApotheke = fee.accepted_fee_pharmacy
                item.akzeptiertARZ = fee.accepted_fee_datacenter
                item.minderungsgruende = fee.reduction_codes()
                item.zeitpunktAkzeptanz = fee.submission_time
            items.append(item)
        return RecordedRequestLog.create(recorded_requests=items)


queryRecordedRequests_method = xsd.Method(
    'queryRecordedRequests', 'https://www.steinreichwald.net/zesar/admin/queryRecordedRequests',
    function=RecordedRequestsQuerier().handle,
    input='queryRequest',
    output='queryResponse',
)

