# -*- coding: utf-8 -*-

from soapfish.core import SOAPError, SOAPRequest

from ..auth import build_who_api


__all__ = ['authenticate_admin']

def authenticate_admin(soap_method):
    def wrapper(*args):
        request = args[1] if isinstance(args[1], SOAPRequest) else args[0]
        environ = request.environ
        settings = environ['settings']
        environ['soap_header'] = request.soap_header
        auth_api = build_who_api(environ, settings, identifier='soap_basic_auth')
        identity = auth_api.authenticate()
        if not identity:
            # Later: return Challenge
            return SOAPError('Client', 'access denied')
        db_user = identity['dbuser']
        if not db_user.is_admin:
            return SOAPError('Client', 'access denied')
        return soap_method(*args)
    return wrapper

