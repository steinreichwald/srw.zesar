# encoding: utf-8

from __future__ import absolute_import, unicode_literals

import threading

from pyramid.settings import asbool
import sqlalchemy
from sqlalchemy.exc import DisconnectionError

from ..models import metadata


__all__ = ['setup_db_sanity_checks', 'DBSanityCheckingMiddleware']

# -----------------------------------------------------------------------------
# code copied from MediaDrop (mediadrop/config/middleware.py)
# all code was written by me (Felix Schwarz) and I hereby relicense the code
# under the MIT license for easier reuse in other projects

class DBSanityChecker(object):
    def __init__(self, check_for_leaked_connections=False,
                 enable_pessimistic_disconnect_handling=False, log=None):
        self._local = threading.local()
        self.is_leak_check_enabled = check_for_leaked_connections
        self.is_alive_check_enabled = enable_pessimistic_disconnect_handling
        self.log = log
        self.pool_listeners = {}
        pool = self._pool()
        self._local.pool_listeners = dict()
        if self.is_leak_check_enabled or self.is_alive_check_enabled:
            sqlalchemy.event.listen(pool, 'checkout', self.on_connection_checkout)
            self._local.pool_listeners['checkout'] = self.on_connection_checkout
        if self.is_leak_check_enabled:
            sqlalchemy.event.listen(pool, 'checkin', self.on_connection_checkin)
            self._local.pool_listeners['checkin'] = self.on_connection_checkin

    def _pool(self):
        engine = metadata.bind
        return engine.pool

    def tear_down(self):
        pool = self._pool()
        for target, fn in self._local.pool_listeners.items():
            if sqlalchemy.event.contains(pool, target, fn):
                sqlalchemy.event.remove(pool, target, fn)

    @property
    def connections(self):
        if not hasattr(self._local, 'connections'):
            self._local.connections = dict()
        return self._local.connections

    def check_for_live_db_connection(self, dbapi_connection):
        # Try to check that the current DB connection is usable for DB queries
        # by issuing a trivial SQL query. It can happen because the user set
        # the 'sqlalchemy.pool_recycle' time too high or simply because the
        # MySQL server was restarted in the mean time.
        # Without this check a user would get an internal server error and the
        # connection would be reset by the DBSessionRemoverMiddleware at the
        # end of that request.
        # This functionality below will prevent the initial "internal server
        # error".
        #
        # This approach is controversial between DB experts. A good blog post
        # (with an even better discussion highlighting pros and cons) is
        # http://www.mysqlperformanceblog.com/2010/05/05/checking-for-a-live-database-connection-considered-harmful/
        #
        # In MediaDrop the check is only done once per request (skipped for
        # static files) so it should be relatively light on the DB server.
        # Also the check can be disabled using the setting
        # 'sqlalchemy.check_connection_before_request = false'.
        #
        # possible optimization: check each connection only once per minute or so,
        # store last check time in private attribute of connection object.

        # code stolen from SQLAlchemy's 'Pessimistic Disconnect Handling' docs
        cursor = dbapi_connection.cursor()
        try:
            cursor.execute('SELECT 1')
        except:
            msg = u'received broken db connection from pool, resetting db session. ' + \
                u'If you see this error regularly and you use MySQL please check ' + \
                u'your "sqlalchemy.pool_recycle" setting (usually it is too high).'
            self.log and self.log.warn(msg)
            # The pool will try to connect again up to three times before
            # raising an exception itself.
            raise DisconnectionError()
        cursor.close()

    def on_connection_checkout(self, dbapi_connection, connection_record, connection_proxy):
        if self.is_alive_check_enabled:
            self.check_for_live_db_connection(dbapi_connection)
        if self.is_leak_check_enabled:
#             if len(self.connections) > 0:
#                 import traceback
#                 traceback.print_stack(limit=15)
            self.connections[id(dbapi_connection)] = True

    def on_connection_checkin(self, dbapi_connection, connection_record):
        connection_id = id(dbapi_connection)
        # connections might be returned *after* this middleware called
        # 'self.connections.clear()', we should not break in that case...
        if connection_id in self.connections:
            del self.connections[connection_id]

    def on_request_done(self, request, tear_down=False):
        leaked_connections = len(self.connections)
        if leaked_connections > 0:
            msg = 'DB connection leakage detected: ' + \
                '%d db connection(s) not returned to the pool' % leaked_connections
            self.log and self.log.error(msg)
            self.connections.clear()
        if tear_down:
            self.tear_down()


class DBSanityCheckingMiddleware(DBSanityChecker):
    def __init__(self, app, *args, **kwargs):
        super(DBSanityCheckingMiddleware, self).__init__(*args, **kwargs)
        self.app = app

    def __call__(self, environ, start_response):
        try:
            return self.app(environ, start_response)
        finally:
            # some frameworks provide a global "request" symbol which could be
            # used to pass a Request object. Pyramid doesn't so we're just
            # passing None here.
            self.on_request_done(None)


def setup_db_sanity_checks(config, app=None, log=None):
    leak_check = asbool(config.get('db.check_for_leaked_connections', False))
    pessimistic_disconnects = asbool(config.get('db.enable_pessimistic_disconnect_handling', False))
    if log:
        bool2str = lambda v: 'enabled' if v else 'disabled'
        params = (bool2str(pessimistic_disconnects), bool2str(leak_check))
        log.debug('pessimistic disconnect handling: %s, leak check: %s' % params)
    if (not leak_check) and (not pessimistic_disconnects):
        return app

    params = dict(
        check_for_leaked_connections=leak_check,
        enable_pessimistic_disconnect_handling=pessimistic_disconnects,
        log=log
    )
    if app is not None:
        return DBSanityCheckingMiddleware(app, **params)
    return DBSanityChecker(**params)
# -----------------------------------------------------------------------------

