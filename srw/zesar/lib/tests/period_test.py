# -*- coding: UTF-8 -*-
#
# The MIT License
#
# Copyright (c) 2011 Felix Schwarz <felix.schwarz@oss.schwarz.eu>
#

from datetime import date
import unittest

from pythonic_testcase import *

from ..period import Period


class PeriodTest(unittest.TestCase):
    # --- Instantiation --------------------------------------------------------
    def test_can_instantiate_period_with_start_and_end(self):
        start = date(2010, 1, 5)
        end = date(2010, 5, 10)
        period = Period(start, end)
        self.assertEquals(start, period.start)
        self.assertEquals(end, period.end)

    # --- Containment/Overlapping ----------------------------------------------
    def test_knows_if_date_is_within_period(self):
        period = Period(date(2010, 1, 5), date(2010, 5, 10))
        assert_true(period.contains(date(2010, 2, 10)))
        assert_false(period.contains(date(2010, 1, 4)))
        assert_false(period.contains(date(2010, 5, 11)))

    def test_supports_in_operator(self):
        period = Period(date(2010, 1, 5), date(2010, 5, 10))
        assert_true(date(2010, 2, 10) in period)
        assert_false(date(2010, 1, 4) in period)
        assert_false(date(2010, 5, 11) in period)

    def test_knows_if_other_period_overlaps(self):
        period = Period(date(2010, 1, 5), date(2010, 5, 10))
        assert_true(period.overlaps(Period(date(2009, 1, 1), date(2010, 1, 5))))
        assert_true(period.overlaps(Period(date(2010, 5, 10), date(2010, 5, 31))))
        assert_true(period.overlaps(Period(date(2010, 2, 1), date(2010, 2, 15))))
        assert_true(period.overlaps(Period(date(2009, 1, 11), date(2011, 1, 1))))

        assert_false(period.overlaps(Period(date(2009, 1, 1), date(2010, 1, 4))))
        assert_false(period.overlaps(Period(date(2010, 5, 11), date(2010, 5, 31))))

    def test_can_tell_if_other_period_is_contained_completely(self):
        period = Period(date(2010, 1, 5), date(2010, 5, 10))
        assert_true(period.contains_completely(period))
        assert_true(period.contains_completely(Period(date(2010, 1, 10), date(2010, 4, 1))))
        
        assert_false(period.contains_completely(Period(date(2010, 1, 4), date(2010, 4, 1))))
        assert_false(period.contains_completely(Period(date(2010, 1, 5), date(2010, 5, 11))))

    # --- periods without end date --------------------------------------------
    def test_supports_periods_without_fixed_end_date(self):
        period = Period(date(2010, 5, 1), end=None)
        assert_false(period.contains(date(2010, 4, 30)))
        assert_true(period.contains(date(2010, 5, 1)))
        assert_true(period.contains(date(2050, 1, 1)))

        q2_2010 = Period(date(2010, 4, 1), date(2010, 6, 30))
        assert_true(period.overlaps(period))
        assert_true(period.overlaps(q2_2010))
        assert_true(q2_2010.overlaps(period))

        assert_true(period.contains_completely(period))
        assert_false(period.contains_completely(q2_2010))
        assert_true(period.contains_completely(Period(date(2011, 1, 1), end=None)))
        assert_false(period.contains_completely(Period(date(2010, 1, 1), end=None)))

    # --- Comparison -----------------------------------------------------------
    def test_can_test_for_inequality(self):
        period = Period(date(2010, 1, 5), date(2010, 5, 10))
        assert_not_none(period)
        assert_not_equals(False, period)
        assert_not_equals([], period)
        
        assert_not_equals(Period(date(2010, 1, 5), date(2010, 5, 3)), period)
        assert_not_equals(Period(date(2010, 1, 6), date(2010, 5, 10)), period)

    def test_can_compare_periods(self):
        period = Period(date(2010, 1, 5), date(2010, 5, 10))
        assert_equals(period, period)
        assert_equals(period, Period(date(2010, 1, 5), date(2010, 5, 10)))

