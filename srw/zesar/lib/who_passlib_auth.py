# encoding: utf-8

from __future__ import absolute_import, unicode_literals

from passlib.context import CryptContext
from repoze.who.interfaces import IAuthenticator
from zope.interface import implementer


__all__ = ['AuthenticationManager', 'DBAuthenticator']

class AuthenticationManager(object):
    def __init__(self, crypt_context, logger=None):
        self.crypt_context = crypt_context
        self.log = logger

    def verify_and_update_password(self, user, password):
        # passlib doesn't accept None so we need to handle that.
        if password is None:
            return False
        try:
            is_valid_password, new_hash = self.crypt_context.verify_and_update(password, user.password_hash)
        except ValueError as e:
            # "hash could not be identified", e.g. when password_hash = ''
            if self.log:
                self.log.info('exception while verifying hash: %s' % e)
            return False
        if is_valid_password and new_hash:
            user.password_hash = new_hash
        return is_valid_password

    def compute_hash(self, password):
        return self.crypt_context.encrypt(password)

    @classmethod
    def init_with_config_string(cls, config_string, logger=None):
        crypt_context = CryptContext.from_string(config_string, section='passlib')
        return AuthenticationManager(crypt_context, logger=logger)

    @classmethod
    def init_with_config_dict(cls, config_dict, logger=None):
        crypt_context = CryptContext(**config_dict)
        return AuthenticationManager(crypt_context, logger=logger)


@implementer(IAuthenticator)
class DBAuthenticator():
    def __init__(self, user_class, auth_manager, logger=None):
        self.user_class = user_class
        self.auth_manager = auth_manager
        self.log = logger

    # IAuthenticator
    def authenticate(self, environ, identity):
        if ('login' not in identity) or ('password' not in identity):
            return None
        username = identity['login']
        password = identity['password']

        user = self.user_class.query\
            .filter(self.user_class.username == username.lower()).first()
        if user is None:
            if self.log:
                self.log.debug('unknown user %r' % username)
            return None
        if self.auth_manager.verify_and_update_password(user, password) != True:
            if self.log:
                self.log.debug('bad password for user %r' % user.username)
            return None
        identity['dbuser'] = user
        return user.username

