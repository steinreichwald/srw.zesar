# encoding: utf-8

from __future__ import absolute_import, unicode_literals

__all__ = ['add_routes']

def add_routes(config):
    config.add_route('zesar_service', '/', request_method='POST')
    config.add_route('zesar_wsdl', '/', request_method='GET', request_param='wsdl')
    config.add_route('zesar_xsd', '/static/xsd/{name}.xsd', request_method='GET',)
    config.add_route('admin_service', '/admin/', request_method='POST')
    config.add_route('admin_wsdl', '/admin/', request_method='GET', request_param='wsdl')

