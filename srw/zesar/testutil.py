# encoding: utf-8

from __future__ import absolute_import, unicode_literals

import binascii
from hashlib import md5
import ssl

from pythonic_testcase import *
from sqlalchemy.engine import create_engine

from .models import Base, DBSession, metadata
from .pyramid_app import make_app

try:
    from webtest import TestApp
    TestApp = TestApp
except ImportError:
    TestApp = None


__all__ = ['zesar_demo_cert', 'zesar_demo_md5', 'DBTestCase', 'FunctionalTestCase', 'TestApp']

class DBTestCase(PythonicTestCase):
    def setUp(self):
        self.metadata = metadata
        self.engine = create_engine('sqlite:///:memory:', echo=False)
        self.metadata.bind = self.engine
        Base.metadata.create_all(self.engine)
        DBSession.configure(bind=self.engine)

    def tearDown(self):
        DBSession.remove()
        self.metadata.drop_all()


class FunctionalTestCase(DBTestCase):
    extra_settings = None

    def setUp(self):
        if TestApp is None:
            self.skipTest('WebTest is not installed')
        super(FunctionalTestCase, self).setUp()
        settings = {
            # During testing a simple/insecure md5 hashing is enough and does
            # not slow down the tests.
            'passlib.schemes': 'hex_md5',
            # sqlite in-memory databases store their contents (tables) per
            # connection so we must pass the engine created in setUp() to the
            # Pyramid app.
            'testing': {
                'engine': self.engine,
            }
        }
        if self.extra_settings:
            settings.update(self.extra_settings)
        zesar_app = make_app({'__file__': None}, **settings)
        environ = {
            'wsgi.url_scheme': 'https',
            'HTTP_HOST': 'srw.example',
            'SCRIPT_NAME':'/foo/bar',
        }
        self.app = TestApp(zesar_app, extra_environ=environ)


zesar_demo_cert = \
"""-----BEGIN CERTIFICATE-----
MIIDmTCCAoGgAwIBAgIEOP+BHDANBgkqhkiG9w0BAQsFADB9MQswCQYDVQQGEwJE
RTEcMBoGA1UECBMTTm9yZHJoZWluLVdlc3RmYWxlbjEOMAwGA1UEBxMFS29lbG4x
EzARBgNVBAoTClpFU0FSIEdtYkgxFDASBgNVBAsTC0VudHdpY2tsdW5nMRUwEwYD
VQQDEwxTYXNjaGEgSm9ja3MwHhcNMTIxMjEyMTQ1MDIxWhcNMjIxMjEwMTQ1MDIx
WjB9MQswCQYDVQQGEwJERTEcMBoGA1UECBMTTm9yZHJoZWluLVdlc3RmYWxlbjEO
MAwGA1UEBxMFS29lbG4xEzARBgNVBAoTClpFU0FSIEdtYkgxFDASBgNVBAsTC0Vu
dHdpY2tsdW5nMRUwEwYDVQQDEwxTYXNjaGEgSm9ja3MwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCxVnTbXWKlom4qXqr/yk3LawEiXzXogn+Wgp75BB4X
CcHYE765tiH7+v/je5QXDqVqbQkfERKZKPcps4nuzBT0FPiQPureJNt+7LmZ5IMW
FIx8abY8SUW43WwuHsV2gqIO7wB1+MY0lk42R1zSwnUk6bLjFAu3UX78u4h78eo2
xjvrxccM4d+lb8M5Ux2vtkanyVDqGh5PL7mtymNg8zdAWXKgaQ5Ls/L55NPDS5D/
3Tpadhf1OQrMzgWpRkG8PDtZVWwY1LjVm5xn7XZ670C9XMtpoTDBu4W3IwTgdqT2
fx7v5CnTAriLa1AmRLAJcZ61DtmGO2ZHn9NUbxf1P5Z9AgMBAAGjITAfMB0GA1Ud
DgQWBBSd73gNXZBsRnkPXYPg1YbehkQAnjANBgkqhkiG9w0BAQsFAAOCAQEAnJSc
IKi/s8FgIdOXcb8seXhwKwp1Ac9qfkZTKGzXQdZYGwW4uyZLn0Jz+Jd4xA24Sedg
UpSA0Kln5S8A8Lb6qKTyKR/3ve/IEfJXl1Xf/KB8E7WOAPHb2s8qqjRAzFaH8r0u
4Q7c9J9aLPSLGxiKL7ZgdFJ19oxsmOdExRlu0cJggzArIlwBGAnBbFG/eJfEl6jH
Oh6HADBw9M6jJ+SUVVHuSYkpZyqY0jHtF5IxnAX7Go/3VngWyw9jw0ZnpUQYI21W
FZYlsU7y63kJ6ws6wny+Gsjxd1ahZ2smNdKZGEbC4KNceCp+04hPBglnUugPQUEe
aBJrEEU00+TvR5MOzg==
-----END CERTIFICATE-----
"""
_der_cert = ssl.PEM_cert_to_DER_cert(zesar_demo_cert)
# bcrypt does not allow null bytes in input so we convert the actual bytes
# into a hex string first.
zesar_demo_md5 = md5(binascii.hexlify(_der_cert)).hexdigest()
