# encoding: utf-8

from __future__ import absolute_import, unicode_literals

import logging

from sqlalchemy import engine_from_config
from pyramid.config import Configurator
from pyramid.events import NewRequest

from .lib.db_sanity_middleware import setup_db_sanity_checks
from .lib.multiconfig import SiteConfig
from .models import Base, DBSession
from .routes_ import add_routes

__all__ = ['main']

def configure_db(settings, engine=None):
    if engine is None:
        engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

def setup_db_for_request(event):
    # In case an exception happened during processing we must roll back the
    # DB session so the next request can use it again.
    def db_session_cleanup(request):
        DBSession.rollback()
        DBSession.close()
    event.request.add_finished_callback(db_session_cleanup)

    # ideally the sanity checker would be set up only once but a quick fix
    # should be good enough right now.
    settings = event.request.registry.settings
    log = logging.getLogger(__name__)
    db_sanity_checker = setup_db_sanity_checks(settings, log=log)
    if db_sanity_checker:
        post_process = lambda *args: db_sanity_checker.on_request_done(tear_down=True, *args)
        event.request.add_finished_callback(post_process)


def main(global_config, **settings):
    test_settings = settings.pop('testing', {})
    config_sources = (
        ('srw.zesar', 'application.ini'),
        settings,
    )
    site_config = SiteConfig(mandatory=config_sources)
    settings = site_config.to_dict('app:main', interpolations={'here': global_config.get('here')})
    settings['site_config'] = site_config.to_string()
    config = Configurator(settings=settings)
    configure_db(settings, engine=test_settings.get('engine'))
    config.add_subscriber(setup_db_for_request, NewRequest)
    add_routes(config)
    config.scan(package='srw.zesar')

    pyramid_app = config.make_wsgi_app()
    return pyramid_app

make_app = main
