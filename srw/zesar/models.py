# encoding: utf-8

from __future__ import absolute_import, unicode_literals

from collections import namedtuple
from datetime import date as DTDate, datetime as DTDateTime

from babel.util import UTC
from pythonic_testcase import assert_true
from schwarz.column_alchemy import UTCDateTime
from sqlalchemy import and_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relationship, scoped_session, sessionmaker
from sqlalchemy.schema import Column, ForeignKey, UniqueConstraint
from sqlalchemy.types import BigInteger, Boolean, Date, Integer, Text, Unicode

from .lib.jsontype_column import JSONType
from .lib.listify import tuplify
from .lib.period import Period


__all__ = [
    'metadata',
    'AcceptedFee', 'DBSession', 'Pharmacy', 'PharmacyPeriod', 'Prescription',
    'RecordedRequest', 'User'
]

DBSession = scoped_session(sessionmaker())
Base = declarative_base()
metadata = Base.metadata


class User(Base):
    __tablename__ = 'users'
    query = DBSession.query_property()

    username = Column(Text, primary_key=True)
    password_hash = Column(Text, nullable=False)
    is_admin = Column(Boolean, default=False)
    is_zesar = Column(Boolean, default=False)

    @classmethod
    def example(cls, **kwargs):
        user = User()
        values = dict(
            username = 'apo12345',
            password_hash = '',
            is_admin = False,
            is_zesar = False,
        )
        values.update(kwargs)
        values['username'] = values['username'].lower()
        for key, value in values.items():
            assert_true(hasattr(user, key))
            setattr(user, key, value)
        DBSession.add(user)
        DBSession.commit()
        return user


# Initially I thought of a "neat" DB schema where the pharmacy id is not the
# primary key and the managed durations are just directly included together
# with address data.
# The advantage of such a scheme would have been that changing pharmacy address
# data would be easily representable in the actual DB.
# The downside of that scheme would have been that naming becomes a bit of an
# issue (there actually no representation of a "Pharmacy" as such in the DB but
# <n> rows which together form the available information about a pharmacy.
# Also having foreign keys (from prescriptions, recorded requests) to "the"
# pharmacy become ambiguous - or they have to refer to unique pharmacy+duration
# rows which means the "natural" pharmacy_id information can not be used as a
# key.
# So in the end I decided that the advantage is not worth the hassle and just
# went with a traditional DB model where we only store one version of address
# data and have a separate table for durations.
#
# However I left the ".info_for()" method in place so later on it should be
# possible to return the correct address data for a given data if that becomes
# necessary.
class Pharmacy(Base):
    __tablename__ = 'pharmacies'
    query = DBSession.query_property()

    pharmacy_id = Column(BigInteger, primary_key=True)
    name = Column(Unicode, nullable=False)
    street = Column(Unicode, nullable=False)
    postal_code = Column(Unicode, nullable=False)
    city = Column(Unicode, nullable=False)
    country_code = Column(Unicode, nullable=False)

    # check class comment for rationale
    def info_for(self, date):
        for db_period in self.periods:
            period = Period(start=db_period.since, end=db_period.until)
            if period.contains(date):
                return self
        return None

    @classmethod
    def example(self, **kwargs):
        defaults = dict(
            pharmacy_id=11223344550,
            name='Rathausapotheke',
            street=' Messedamm 56',
            postal_code='01171',
            city='Dresden',
            country_code='de',
        )
        if 'periods' in kwargs:
            kwargs['periods'] = list(kwargs['periods'])
        else:
            defaults['periods'] = [PharmacyPeriod(since=DTDate(1999, 1, 1), until=None)]
        defaults.update(kwargs)
        pharmacy = Pharmacy()
        for key, value in defaults.items():
            assert hasattr(pharmacy, key)
            setattr(pharmacy, key, value)
        DBSession.add(pharmacy)
        DBSession.flush()
        return pharmacy


class PharmacyPeriod(Base):
    __tablename__ = 'pharmacy_periods'
    query = DBSession.query_property()

    id = Column(Integer, autoincrement=True, primary_key=True)
    pharmacy_id = Column(BigInteger, ForeignKey('pharmacies.pharmacy_id'), nullable=False)
    since = Column(Date, nullable=False)
    until = Column(Date, nullable=True)
    pharmacy = relationship(Pharmacy, backref=backref('periods', order_by=since))


PrescriptionItem = namedtuple('PrescriptionItem', ('pzn', 'faktor'))

class Prescription(Base):
    __tablename__ = 'prescriptions'
    __table_args__ = (
        # The (pRezept) transaction number is only loosely specified and we
        # can not rely on them being unique forever (per pharmacy of course).
        # We're going on the safe side by enforcing uniqueness only per year.
        UniqueConstraint('pharmacy_id', 'transaction_number', 'year',
            name='pharmacy_id_transaction_number_year_key'),
    )
    query = DBSession.query_property()

    id = Column(Integer, autoincrement=True, primary_key=True)
    pharmacy_id = Column(BigInteger, ForeignKey('pharmacies.pharmacy_id'), nullable=False)
    year = Column(Integer, nullable=False)
    # pRezept: "Erstellungszeitpunkt"
    # ZESAR: "Ausgabedatum"
    delivery_date = Column(Date, nullable=False)
    pharmacy = relationship(Pharmacy, backref=backref('prescriptions', order_by=delivery_date))

    # pRezept: "Transaktionsnummer"
    # ZESAR: "TAN"
    transaction_number = Column(BigInteger, nullable=False)
    special_code = Column(BigInteger, nullable=False) # "Sonderkennzeichen"
    valuation = Column(Integer, nullable=False) # "Taxe"
    currency = Column(Unicode(3), nullable=False)
    data = Column(JSONType, nullable=False)

    @tuplify
    def positions(self):
        if self.data is not None:
            for position in self.data.get('positions', ()):
                yield PrescriptionItem(pzn=position['pzn'], faktor=position['faktor'])

    @classmethod
    def get_or_add(cls, **primary_keys):
        conditions = []
        for key, value in primary_keys.items():
            column = getattr(cls, key)
            conditions.append(column == value)
        prescription = cls.query.filter(and_(*conditions)).first()
        if prescription is None:
            prescription = Prescription()
            for key, value in primary_keys.items():
                assert hasattr(prescription, key)
                setattr(prescription, key, value)
        DBSession.add(prescription)
        return prescription

    @classmethod
    def example(self, **kwargs):
        defaults = dict(
            transaction_number=987654321,
            special_code=7777777,
            valuation=10,
            delivery_date=DTDate(2014, 5, 2),
            # pharmacy_id
            currency='EUR',
            data={'positions': []},
        )
        if ('pharmacy_id' not in kwargs) and ('pharmacy' not in kwargs):
            defaults['pharmacy'] = Pharmacy.example()
        defaults.update(kwargs)
        if ('year' not in defaults):
            defaults['year'] = defaults['delivery_date'].year
        prescription = Prescription()
        for key, value in defaults.items():
            assert hasattr(prescription, key), 'unknown key %r' % key
            setattr(prescription, key, value)
        DBSession.add(prescription)
        DBSession.flush()
        return prescription


class RecordedRequest(Base):
    __tablename__ = 'recorded_requests'
    query = DBSession.query_property()

    record_id = Column(BigInteger, primary_key=True)
    pharmacy_id = Column(BigInteger, ForeignKey('pharmacies.pharmacy_id'), nullable=False)
    transaction_number = Column(BigInteger, nullable=False)
    # The prescription relation is nullable so prescriptions can be purged from
    # the server without breaking any DB constraints.
    prescription_id = Column(Integer, ForeignKey('prescriptions.id'), nullable=True)
    request_time = Column(UTCDateTime, nullable=False)
    # Store the username for each recorded request. Billing information only
    # returns recorded requests for the current user. This is done so we can
    # have test users which request information as well but of course we must
    # not bill ZESAR for these test requests.
    username = Column(Text, ForeignKey('users.username'), nullable=False)

    prescription = relationship(Prescription, backref=backref('recorded_requests', order_by=request_time))
    pharmacy = relationship(Pharmacy, backref=backref('recorded_requests', order_by=request_time))
    user = relationship(User, backref=backref('recorded_requests', order_by=request_time))

    @classmethod
    def example(self, **kwargs):
        defaults = dict(
            record_id=123456,
            transaction_number=987654321,
            request_time=DTDateTime(2014, 11, 5, 10, 32, tzinfo=UTC),
        )
        if ('pharmacy_id' not in kwargs) and ('pharmacy' not in kwargs):
            defaults['pharmacy'] = Pharmacy.example()
        if ('user_id' not in kwargs) and ('user' not in kwargs):
            defaults['user'] = User.example()
        defaults.update(kwargs)
        request = RecordedRequest()
        for key, value in defaults.items():
            assert hasattr(request, key), 'unknown key %r' % key
            setattr(request, key, value)
        DBSession.add(request)
        DBSession.flush()
        return request


class AcceptedFee(Base):
    __tablename__ = 'accepted_fees'
    query = DBSession.query_property()

    record_id = Column(BigInteger,
        ForeignKey('recorded_requests.record_id'),
        primary_key=True
    )
    transaction_number = Column(BigInteger, nullable=False)
    pharmacy_id = Column(BigInteger, ForeignKey('pharmacies.pharmacy_id'), nullable=False)
    requested_fee_pharmacy = Column(BigInteger, nullable=False)
    requested_fee_datacenter = Column(BigInteger, nullable=False)
    accepted_fee_pharmacy = Column(BigInteger, nullable=False)
    accepted_fee_datacenter = Column(BigInteger, nullable=False)
    data = Column(JSONType, nullable=False)
    submission_time = Column(UTCDateTime, nullable=False)
    submitting_user = Column(Text, ForeignKey('users.username'), nullable=False)

    pharmacy = relationship(Pharmacy)
    recorded_request = relationship(RecordedRequest, backref=backref('accepted_fees', order_by=submission_time))
    user = relationship(User, backref=backref('accepted_fees', order_by=submission_time))

    @classmethod
    def example(self, **kwargs):
        defaults = dict(
            record_id=123458,
            transaction_number=987654329,
            submission_time=DTDateTime(2014, 11, 5, 10, 32, tzinfo=UTC),
            data={'reduction_codes': ()}
        )
        if ('pharmacy_id' not in kwargs) and ('pharmacy' not in kwargs):
            defaults['pharmacy'] = Pharmacy.example()
        if ('submitting_user' not in kwargs) and ('user' not in kwargs):
            defaults['user'] = User.example()
        defaults.update(kwargs)
        accepted_fee = AcceptedFee()
        for key, value in defaults.items():
            assert hasattr(accepted_fee, key), 'unknown key %r' % key
            setattr(accepted_fee, key, value)
        DBSession.add(accepted_fee)
        DBSession.flush()
        return accepted_fee

    def reduction_codes(self):
        if self.data is not None:
            return tuple(self.data.get('reduction_codes', ()))

