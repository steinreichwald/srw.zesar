# -*- coding: utf-8 -*-

import calendar
from datetime import date as Date, datetime as DateTime
import re

import pytz
from soapfish import soap11, xsd
from soapfish.core import SOAPError
from sqlalchemy import and_

from ...auth import current_username
from ...lib.period import Period
from ...models import RecordedRequest
from ..common_types import *
from .response_types import RechnungsdatenAntwort, RechPosition


__all__ = ['sendeRechnungsdaten_method']

def dt_period(year, month):
    berlin = pytz.timezone('Europe/Berlin')
    start_of_month = DateTime(year, month, 1, hour=0, minute=0, second=0)
    last_day = calendar.monthrange(year, month)[1]
    end_of_month = DateTime(year, month, last_day, hour=23, minute=59, second=59, microsecond=999999)
    return Period(start=berlin.localize(start_of_month), end=berlin.localize(end_of_month))

def date_period(year, month):
    start_of_month = Date(year, month, 1)
    last_day = calendar.monthrange(year, month)[1]
    end_of_month = Date(year, month, last_day)
    return Period(start=start_of_month, end=end_of_month)


def sendeRechnungsdaten(request, rechnungsdatenAnfrage):
    invoice_month = rechnungsdatenAnfrage.abrechnungsmonat
    match = re.search('^(\d{4})(\d{2})$', invoice_month)
    year = int(match.group(1))
    month = int(match.group(2))
    if not (1 <= month <= 12):
        msg = 'Invalid invoicing period %r specified.' % invoice_month
        return SOAPError(soap11.Code.CLIENT, msg)

    invoice_period = dt_period(year, month)
    condition = and_(
        RecordedRequest.request_time >= invoice_period.start,
        RecordedRequest.request_time <= invoice_period.end,
        RecordedRequest.username == current_username(request.environ),
    )
    positions = []
    june2011 = date_period(2011, 6)
    for recorded_request in RecordedRequest.query.filter(condition):
        apo_compensation = 200
        prescription = recorded_request.prescription
        if prescription is not None:
            # prescription might be None in tests but also if we deleted a
            # prescription in the database to minimize the data in our online
            # servers.
            # (alternatively we could extend the DB schema so it contains the
            # delivery date)
            delivery_date = prescription.delivery_date
            # account for increased APO compensation for prescriptions delivered
            # in June 2011 as specified in the ZESAR API 1.0 specs (2012-08-21)
            #    3.2.3.2 Ausgabedaten (Rechnungsdaten), page 20/21
            if june2011.contains(delivery_date):
                apo_compensation = 300
        position = RechPosition.create(
            tan='%09d' % recorded_request.transaction_number,
            apoIK='%09d' % recorded_request.pharmacy_id,
            recordID=recorded_request.record_id,
            forderungApo=apo_compensation,
            forderungArz=100,
        )
        positions.append(position)
    return RechnungsdatenAntwort.create(positionen=positions)


sendeRechnungsdaten_method = xsd.Method(
    'sendeRechnungsdaten', 'http://www.zesargmbh.com/ZesarRz/sendeRechnungsdaten',
    function=sendeRechnungsdaten,
    input='rechnungsdatenAnfrage',
    inputPartName='rechnungsdatenAnfrage',
    output='rechnungsdatenAntwort',
    outputPartName='rechnungsdatenAntwort',
)

