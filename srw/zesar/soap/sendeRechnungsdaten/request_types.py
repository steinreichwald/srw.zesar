# -*- coding: utf-8 -*-

from soapfish import xsd

from ..common_types import create_
from ..definitions import xsd_baseurl


__all__ = ['RechnungsdatenAnfrage', 'RechnungsdatenAnfrageSchema']

class AbrechnungsmonatTyp(xsd.String):
    pattern = r'[0-9]{6}'


class RechnungsdatenAnfrage(xsd.ComplexType):
    abrechnungsmonat = xsd.Element(AbrechnungsmonatTyp)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


RechnungsdatenAnfrageSchema = xsd.Schema(
    targetNamespace='http://zesargmbh.com/zesar/schema/rechnungsdatenAnfrage/v1_0_0',
    location=xsd_baseurl + 'Schema_rechnungsdatenAnfrage_V1.0.0.xsd',
    elementFormDefault=xsd.ElementFormDefault.QUALIFIED,
    simpleTypes=(AbrechnungsmonatTyp, ),
    complexTypes=(RechnungsdatenAnfrage, ),
    elements={
        'rechnungsdatenAnfrage': xsd.Element(RechnungsdatenAnfrage)
    },
)
