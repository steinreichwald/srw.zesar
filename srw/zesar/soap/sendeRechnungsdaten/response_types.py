# -*- coding: utf-8 -*-

from soapfish import xsd

from ..common_types import create_, IKNrTyp, TANTyp
from ..definitions import xsd_baseurl


__all__ = ['RechnungsdatenAntwort', 'RechnungsdatenAntwortSchema']

class RechPosition(xsd.ComplexType):
    tan = xsd.Element(TANTyp)
    apoIK = xsd.Element(IKNrTyp)
    recordID = xsd.Element(xsd.Long)
    forderungApo = xsd.Element(xsd.Long(totalDigits=15))
    forderungArz = xsd.Element(xsd.Long(totalDigits=15))

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


class RechnungsdatenAntwort(xsd.ComplexType):
    positionen = xsd.ListElement(RechPosition, 'rechPosition', minOccurs=0, maxOccurs=xsd.UNBOUNDED)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


RechnungsdatenAntwortSchema = xsd.Schema(
    'http://zesargmbh.com/zesar/schema/rechnungsdatenAntwort/v1_0_0',
    location=xsd_baseurl + 'Schema_rechnungsdatenAntwort_V1.0.0.xsd',
    elementFormDefault=xsd.ElementFormDefault.QUALIFIED,
    simpleTypes=(IKNrTyp, TANTyp),
    complexTypes=(RechnungsdatenAntwort, RechPosition),
    elements={
        'rechnungsdatenAntwort': xsd.Element(RechnungsdatenAntwort),
    },
)
