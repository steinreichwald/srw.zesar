# -*- coding: utf-8 -*-

from soapfish import xsd

from ..common_types import create_, IKNrTyp, TANTyp
from ..definitions import xsd_baseurl


__all__ = [
    'AkzeptRechnungsdatenLieferung',
    'AkzeptRechnungsdatenLieferungSchema',
    'AkzeptRechPosition',
]

class AkzeptRechPosition(xsd.ComplexType):
    tan = xsd.Element(TANTyp)
    apoIK = xsd.Element(IKNrTyp)
    recordID = xsd.Element(xsd.Long)
    forderungApo = xsd.Element(xsd.Long(totalDigits=14))
    forderungArz = xsd.Element(xsd.Long(totalDigits=14))
    akzeptiertApo = xsd.Element(xsd.Long(totalDigits=14))
    akzeptiertArz = xsd.Element(xsd.Long(totalDigits=14))
    minGruende = xsd.ListElement(
        xsd.Short(totalDigits=3, minInclusive=-999, maxInclusive=999),
        'minGrund',
        minOccurs=0,
        maxOccurs=20
    )

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


class AkzeptRechnungsdatenLieferung(xsd.ComplexType):
    positionen = xsd.ListElement(AkzeptRechPosition, 'akzeptRechPosition', minOccurs=1, maxOccurs=xsd.UNBOUNDED)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


AkzeptRechnungsdatenLieferungSchema = xsd.Schema(
    'http://zesargmbh.com/zesar/schema/akzeptRechnungsdatenLieferung/v1_0_0',
    location=xsd_baseurl + 'Schema_akzeptRechnungsdatenLieferung_V1.0.0.xsd',
    elementFormDefault=xsd.ElementFormDefault.QUALIFIED,
    simpleTypes=[IKNrTyp, TANTyp],
    complexTypes=(AkzeptRechnungsdatenLieferung, AkzeptRechPosition),
    elements={
        'akzeptRechnungsdatenLieferung': xsd.Element(AkzeptRechnungsdatenLieferung),
    },
)
