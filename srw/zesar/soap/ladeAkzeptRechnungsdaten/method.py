# -*- coding: utf-8 -*-

from datetime import datetime as DateTime

from babel.util import LOCALTZ
from soapfish import xsd

from ...auth import current_username
from ...models import AcceptedFee, DBSession
from ..common_types import *
from .response_types import *


__all__ = ['ladeAkzeptRechnungsdaten_method']

def ladeAkzeptRechnungsdaten(request, akzeptRechnungsdatenLieferung):
    now = DateTime.now(tz=LOCALTZ)
    item_count = 0
    accepted_pharmacy = 0
    accepted_datacenter = 0
    for position in akzeptRechnungsdatenLieferung.positionen:
        fee = AcceptedFee(
            record_id=position.recordID,
            transaction_number=int(position.tan),
            pharmacy_id=int(position.apoIK),
            requested_fee_pharmacy=position.forderungApo,
            requested_fee_datacenter=position.forderungArz,
            accepted_fee_pharmacy=position.akzeptiertApo,
            accepted_fee_datacenter=position.akzeptiertArz,
            submission_time=now,
            submitting_user=current_username(request.environ),
            data={'reduction_codes': tuple(position.minGruende)},
        )
        DBSession.add(fee)
        item_count += 1
        accepted_pharmacy += position.akzeptiertApo
        accepted_datacenter += position.akzeptiertArz
    result = AkzeptRechnungsdatenQuittung.create(
        anzDs=item_count,
        summeApo=accepted_pharmacy,
        summeArz=accepted_datacenter,
    )
    DBSession.commit()
    return result


ladeAkzeptRechnungsdaten_method = xsd.Method(
    function=ladeAkzeptRechnungsdaten,
    soapAction='http://www.zesargmbh.com/ZesarRz/ladeAkzeptRechnungsdaten',
    input='akzeptRechnungsdatenLieferung',
    inputPartName='akzeptRechnungsdatenLieferung',
    output='akzeptRechnungsdatenQuittung',
    outputPartName='akzeptRechnungsdatenQuittung',
    operationName='ladeAkzeptRechnungsdaten',
)

