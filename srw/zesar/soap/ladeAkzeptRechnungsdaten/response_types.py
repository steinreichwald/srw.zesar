# -*- coding: utf-8 -*-

from soapfish import xsd

from ..common_types import create_
from ..definitions import xsd_baseurl


__all__ = ['AkzeptRechnungsdatenQuittung', 'AkzeptRechnungsdatenQuittungSchema']


class AkzeptRechnungsdatenQuittung(xsd.ComplexType):
    anzDs = xsd.Element(xsd.Integer)
    summeApo = xsd.Element(xsd.Integer(totalDigits=14))
    summeArz = xsd.Element(xsd.Integer(totalDigits=14))

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


AkzeptRechnungsdatenQuittungSchema = xsd.Schema(
    'http://zesargmbh.com/zesar/schema/akzeptRechnungsdatenQuittung/v1_0_0',
    location=xsd_baseurl + 'Schema_akzeptRechnungsdatenQuittung_V1.0.0.xsd',
    elementFormDefault=xsd.ElementFormDefault.QUALIFIED,
    complexTypes=(AkzeptRechnungsdatenQuittung, ),
    elements={
        'akzeptRechnungsdatenQuittung': xsd.Element(AkzeptRechnungsdatenQuittung)
    },
)
