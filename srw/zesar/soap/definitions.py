# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals


__all__ = ['ws_location', 'xsd_baseurl', 'zesar_namespace']

ws_location = 'https://zesar.steinreichwald.net/'
xsd_baseurl = ws_location + 'static/xsd/'
zesar_namespace = 'http://zesargmbh.com/zesar/service'
