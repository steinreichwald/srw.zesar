# -*- coding: utf-8 -*-

from soapfish import xsd

from ..common_types import create_, IKNrTyp, TANTyp
from ..definitions import xsd_baseurl


__all__ = ['ParenteraliaAnfrage', 'ParenteraliaAnfrageSchema', 'ParenteraliaSatzAnfrage']

class ParenteraliaSatzAnfrage(xsd.ComplexType):
    tan = xsd.Element(TANTyp)
    abgabedatum = xsd.Element(xsd.Date)
    apoIK = xsd.Element(IKNrTyp)
    recordID = xsd.Element(xsd.Long)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


class ParenteraliaAnfrage(xsd.ComplexType):
    parenteralia_saetze = xsd.ListElement(ParenteraliaSatzAnfrage, 'parenteraliaSatz', minOccurs=0, maxOccurs=5000)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


ParenteraliaAnfrageSchema = xsd.Schema(
    targetNamespace='http://zesargmbh.com/zesar/schema/parenteraliaAnfrage/v1_0_0',
    location=xsd_baseurl + 'Schema_parenteraliaAnfrage_V1.0.0.xsd',
    elementFormDefault=xsd.ElementFormDefault.QUALIFIED,
    simpleTypes=[IKNrTyp, TANTyp],
    complexTypes=[ParenteraliaAnfrage, ParenteraliaSatzAnfrage],
    elements={
        'parenteraliaAnfrage': xsd.Element(ParenteraliaAnfrage),
        'parenteraliaSatz': xsd.Element(ParenteraliaSatzAnfrage),
    },
)

