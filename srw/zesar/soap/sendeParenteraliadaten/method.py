# -*- coding: utf-8 -*-

from __future__ import absolute_import

from datetime import datetime as DateTime
import logging
import warnings

from babel.dates import format_date
from babel.util import LOCALTZ
from sqlalchemy import and_
from soapfish import xsd

from ...auth import current_username
from ...models import DBSession, Pharmacy, Prescription, RecordedRequest
from .response_types import (Einzelbestandteil, FehlerSatz,
    ParenteraliaAntwort, ParenteraliaSatzAntwort)


__all__ = ['sendeParenteraliadaten_method']

def pad(string, min=0, max=1, char='0'):
    if not isinstance(string, str):
        string = str(string)
    if len(string) > max:
        warnings.warn('String %r exceeds maximum length %d' % (string, max))
    if len(string) >= min:
        return string[:max]
    pad_chars = min - len(string)
    return (char * pad_chars) + string


class ParenteraliaResponder(object):
    def __init__(self):
        self.log = logging.getLogger(__name__)

    def handle(self, request, parenteraliaAnfrage):
        results = []
        for item in parenteraliaAnfrage.parenteralia_saetze:
            results.append(self._handle_parenteralia_request(item, request.environ))
        response = ParenteraliaAntwort.create(parenteralia_antworten=results)
        DBSession.commit()
        return response

    def _error_unknown_pharmacy(self, parenteralia_request):
        return self._zesar_error(parenteralia_request, 9200, 'unbekannte Apotheke')

    def _error_pharmacy_not_managed_at_date(self, parenteralia_request):
        return self._zesar_error(parenteralia_request, 9201, 'Apotheke in diesem Abrechnungszeitraum unbekannt')

    def _error_unknown_tan(self, parenteralia_request):
        return self._zesar_error(parenteralia_request, 9100, 'TAN nicht gefunden')

    def _zesar_error(self, parenteralia_request, error_code, error_message):
        return FehlerSatz.create(
            tan=parenteralia_request.tan,
            apoIK=parenteralia_request.apoIK,
            fehlercode=error_code,
            parameter=error_message,
            recordID=parenteralia_request.recordID
        )

    def _handle_parenteralia_request(self, item, environ):
        pharmacy_id = int(item.apoIK)
        transaction_number = int(item.tan)
        prescriptions = Prescription.query.filter(
            and_(
                Prescription.pharmacy_id == pharmacy_id,
                Prescription.transaction_number == transaction_number,
            )
        )
        if prescriptions.count() > 0:
            now = DateTime.now(tz=LOCALTZ)
            prescription = self._find_closest_prescription(prescriptions, item.abgabedatum.as_datetime_date())
            if not self._request_already_recorded(item.recordID, transaction_number, pharmacy_id):
                recorded_request = RecordedRequest(
                    record_id=item.recordID,
                    pharmacy_id=pharmacy_id,
                    transaction_number=transaction_number,
                    prescription_id=prescription.id,
                    request_time=now,
                    username=current_username(environ),
                )
                DBSession.add(recorded_request)
            return self._parenteralia_item(prescription, item.recordID)

        pharmacy = Pharmacy.query.filter(Pharmacy.pharmacy_id == int(item.apoIK)).first()
        delivery_date = item.abgabedatum.as_datetime_date()
        if pharmacy is None:
            return self._error_unknown_pharmacy(item)
        elif pharmacy.info_for(delivery_date) is None:
            # According to the ZESAR 1.0 spec (2012-08-21) the TAN is
            # the primary lookup key for a prescription and the date
            # can be different. The spec text does not contain any
            # provisions for a max date difference.
            # > Ist kein zur TAN korrespondierender Parenteraliadatensatz
            # > vorhanden, meldet das ARZ den entsprechenden Fehlergrund (...)
            # > in einem Fehlerdatensatz zurück. Das Abgabedatum im
            # > Datensatz könnte vom Abgabedatum auf dem Rezept abweichen.
            # > Insofern gilt die TAN als Hauptkriterium zur Ermittlung
            # > eines Parenteraliadatensatzes. Da die TAN jedoch nicht
            # > über mehrere Jahre eindeutig ist, wird das Abgabedatum
            # > als weiteres Kriterium zur Unterscheidung identischer
            # > TAN herangezogen.
            return self._error_pharmacy_not_managed_at_date(item)
        else:
            msg = 'Keine Rezeptdaten vorhanden für TAN %s (%s), ApoIK %s'
            date_string = format_date(delivery_date, format='short', locale='de_DE')
            self.log.info(msg % (transaction_number, date_string, item.apoIK))
            return self._error_unknown_tan(item)

    def _request_already_recorded(self, record_id, transaction_number, pharmacy_id):
        recorded_request = RecordedRequest.query.filter(
            and_(
                RecordedRequest.record_id == record_id,
                RecordedRequest.transaction_number == transaction_number,
                RecordedRequest.pharmacy_id == pharmacy_id,
            )
        ).first()
        if recorded_request is None:
            return False
        # It might seem that this case should never happen as the ZESAR
        # specification (v1.0) says:
        # > 3 Übertragene Daten
        # > 3.1.1  Webservice „sendeParenteraliadaten“
        # > Da dieselbe TAN (theoretisch auch zeitgleich) sowohl durch ein VU
        # > als auch durch eine BHS angefragt werden kann, kann ein
        # > TAN-Datensatz mit gleicher TAN, gleichem Abgabedatum und gleichem
        # > Apotheken-IK in einem Webservice-Aufruf mehrmals enthalten sein.
        # > Die TAN-Datensätze haben dann jedoch eine unterschiedliche
        # > Record-ID. Die Record-ID ist die eindeutige ID für eine TAN-Abfrage.
        #
        # However the Innovas test suite can send a record id twice (parameters
        # tan+apoIK exactly the same as in the first request).
        # This can happen if the response from our server got lost and they
        # retry the transmission using the same parameters.
        # In this case we should not freak out but just return the actual
        # prescription data without recording another transmission.
        # This is no disadvantage for us as billing is calculated based on
        # *distinct* prescriptions transmitted (and not number of
        # transmissions).
        msg = 'Duplicate record id %r in sendeParenteraliadaten: tan=%s, apoIK=%s (not recording another transmission)'
        self.log.warn(msg % (record_id, transaction_number, pharmacy_id))
        return True

    def _find_closest_prescription(self, prescriptions, requested_date):
        prescription = None
        closest_difference = None
        for prescription_ in prescriptions:
            delivery_date = prescription_.delivery_date
            difference = abs(delivery_date - requested_date)
            if (closest_difference is None) or (difference < closest_difference):
                prescription = prescription_
                closest_difference = difference
        return prescription

    def _parenteralia_item(self, prescription, record_id):
        components = []
        for position in prescription.positions():
            if position.faktor == 0:
                continue
            component = Einzelbestandteil.create(
                pzn=pad(position.pzn, min=7, max=8),
                faktor=position.faktor
            )
            components.append(component)

        pharmacy = prescription.pharmacy
        return ParenteraliaSatzAntwort.create(
            recordID=record_id,
            tan=pad(str(prescription.transaction_number), min=9, max=9),
            sonderkennzeichen=pad(str(prescription.special_code), min=7, max=8),
            taxe=prescription.valuation,
            abgabedatum=prescription.delivery_date,
            waehrungskennzeichen=prescription.currency,
            einzelbestandteile=components,
            apoIK=pad(pharmacy.pharmacy_id, min=9, max=9, char='0'),
            apoName=pad(pharmacy.name, max=60),
            apoStrHausnum=pharmacy.street[:75],
            apoPLZ=pad(pharmacy.postal_code, min=5, max=5),
            apoOrt=pad(pharmacy.city, max=75),
            apoLaenderkennzeichen=pharmacy.country_code[:3],
        )


sendeParenteraliadaten_method = xsd.Method(
    operationName='sendeParenteraliadaten',
    soapAction='http://www.zesargmbh.com/ZesarRz/sendeParenteraliadaten',
    function=ParenteraliaResponder().handle,
    input='parenteraliaAnfrage',
    inputPartName='parenteraliaAnfrage',
    output='parenteraliaAntwort',
    outputPartName='parenteraliaAntwort',
)

