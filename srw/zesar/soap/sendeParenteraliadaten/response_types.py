# -*- coding: utf-8 -*-

from soapfish import xsd

from ..common_types import create_, IKNrTyp, TANTyp
from ..definitions import xsd_baseurl
from .. import xsd_


__all__ = [
    'FehlerSatz', 'ParenteraliaAntwort', 'ParenteraliaAntwortSchema',
    'ParenteraliaSatzAntwort'
]

class FehlerSatz(xsd.ComplexType):
    tan = xsd.Element(TANTyp)
    apoIK = xsd.Element(IKNrTyp)
    fehlercode = xsd.Element(xsd.Integer(totalDigits=4))
    parameter = xsd.Element(xsd.String(pattern='.{0,200}')) # maxLength=200
    recordID = xsd.Element(xsd.Long)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


class SonderkennzeichenTyp(xsd.String):
    pattern = r'[0-9]{7,8}'


class PlzTyp(xsd.String):
    pattern = r'[0-9]{5}'


class PznTyp(xsd.String):
    pattern = r'[0-9]{7,8}'


class Einzelbestandteil(xsd.ComplexType):
    pzn = xsd.Element(PznTyp)
    faktor = xsd.Element(xsd.Integer(totalDigits=10))

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


class ParenteraliaSatzAntwort(xsd.ComplexType):
    tan = xsd.Element(TANTyp)
    sonderkennzeichen = xsd.Element(SonderkennzeichenTyp) # nach TA1, bestimmt Art der Rezeptur
    taxe = xsd.Element(xsd.Integer(totalDigits=14)) # in Euro-Cent
    abgabedatum = xsd.Element(xsd.Date)
    apoIK = xsd.Element(IKNrTyp)
    apoName = xsd.Element(xsd.String(pattern='.{0,60}')) # maxLength=60
    apoStrHausnum = xsd.Element(xsd.String(pattern='.{0,75}')) # maxLength=75
    apoPLZ = xsd.Element(PlzTyp)
    apoLaenderkennzeichen = xsd.Element(xsd.String(pattern='.{0,3}')) # maxLength=3
    apoOrt = xsd.Element(xsd.String(pattern='.{0,75}')) # maxLength=75
    waehrungskennzeichen = xsd.Element(xsd.String(pattern='.{3}')) # length=3
    recordID = xsd.Element(xsd.Long)
    einzelbestandteile = xsd.ListElement(
        Einzelbestandteil,
        'einzelbestandteil',
        minOccurs=0,
        maxOccurs=xsd.UNBOUNDED
    )

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)

class ParenteraliaAntwort(xsd_.ChoiceContainer):
    parenteralia_antworten = xsd_.ChoiceList({
            ParenteraliaSatzAntwort: 'parenteraliaSatz',
            FehlerSatz: 'fehlerSatz',
        }, minOccurs=0, maxOccurs=5000
    )

    @classmethod
    def create(cls, parenteralia_antworten):
        instance = cls()
        instance.parenteralia_antworten = parenteralia_antworten
        return instance


ParenteraliaAntwortSchema = xsd.Schema(
    'http://zesargmbh.com/zesar/schema/parenteraliaAntwort/v1_0_0',
    location=xsd_baseurl + 'Schema_parenteraliaAntwort_V1.0.0.xsd',
    elementFormDefault=xsd.ElementFormDefault.QUALIFIED,
    simpleTypes=(
        IKNrTyp,
        PlzTyp,
        PznTyp,
        TANTyp,
        SonderkennzeichenTyp,
    ),
    complexTypes=(
        Einzelbestandteil,
        FehlerSatz,
        ParenteraliaAntwort,
        ParenteraliaSatzAntwort,
    ),
    elements={
        'parenteraliaAntwort': xsd.Element(ParenteraliaAntwort),
        'parenteraliaSatz': xsd.Element(ParenteraliaSatzAntwort),
        'einzelbestandteil': xsd.Element(Einzelbestandteil),
        'fehlerSatz': xsd.Element(FehlerSatz),
    },
)
