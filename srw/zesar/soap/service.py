# -*- coding: utf-8 -*-

from __future__ import absolute_import

from soapfish import soap, xsd

from .definitions import ws_location, zesar_namespace
from .ladeAkzeptRechnungsdaten import *
from .sendeParenteraliadaten import *
from .sendeRechnungsdaten import *


__all__ = ['ZesarSchema', 'ZesarService']

ZesarSchema = xsd.Schema(
    targetNamespace=zesar_namespace,
    elementFormDefault=xsd.ElementFormDefault.UNQUALIFIED,
    imports=[
        AkzeptRechnungsdatenLieferungSchema,
        AkzeptRechnungsdatenQuittungSchema,
        ParenteraliaAnfrageSchema,
        ParenteraliaAntwortSchema,
        RechnungsdatenAnfrageSchema,
        RechnungsdatenAntwortSchema,
    ],
)

################################################################################
# SOAP Service

ZesarService = soap.Service(
    name='ZesarRzServiceSoapPort',
    targetNamespace=zesar_namespace,
    location=ws_location,
    schemas=(ZesarSchema,),
    version=soap.SOAPVersion.SOAP11,
    methods=[
        ladeAkzeptRechnungsdaten_method,
        sendeParenteraliadaten_method,
        sendeRechnungsdaten_method,
    ],
)
