# -*- coding: utf-8 -*-

from lxml import etree
from soapfish import xsd

__all__ = ['ChoiceContainer', 'ChoiceList']

# The current soapfish model code does not allow representing XSD types like:
# <xs:complexType>
#     <xs:choice maxOccurs="5000">
#         <xs:element ref="tns:foo"/>
#         <xs:element ref="tns:bar"/>
#     </xs:choice>
# </xs:complexType>
# which we need for parenterialiaAntwort.
# The main problem is representing the idea of 'alternatives' where one model
# field generates two entries in the schema, but serializes only one of the
# fields and accepts either value.
# I hacked soapfish so it has a new method '.xsd_types()' (a no-op for
# soapfish's built-in types) which is used to generate the XSD schema correctly
# (soapfish will auto-generate a schema for validation in several places).
# All of the parsing/XML serialization code is contained here because I don't
# know how to build this "the right way" in soapfish proper.
# Right now this seems to be a "good enough" workaround.


class ChoiceContainer(xsd.ComplexType):
    INDICATOR = xsd.Choice
    def __new__(cls, *args, **kwargs):
        instance = super(ChoiceContainer, cls).__new__(cls)
        for field in instance._meta.all:
            setattr(instance, field._name, field.empty_value())
        return instance

    @classmethod
    def parse_xmlelement(cls, xmlelement):
        instance = cls()
        instance._xmlelement = xmlelement
        for attribute in instance._meta.attributes:
            attribute.parse(instance, attribute._name, xmlelement)

        for field in instance._meta.fields:
            for choice_field in field.xsd_elements():
                subelements = cls._find_subelement(choice_field, xmlelement)
                if subelements:
                    break
            if subelements:
                for subelement in subelements:
                    field.parse(instance, field._name, subelement)
                break

        for group in instance._meta.groups:
            group.parse(instance, group._name, xmlelement)
        return instance


class ChoiceList(xsd.ListElement):
    def __init__(self, class_mapping, minOccurs=None, maxOccurs=None, nillable=False, namespace=None):
        for element in class_mapping:
            if not hasattr(element, '_creation_number'):
                element._creation_number = 0
        random_klass = tuple(class_mapping.keys())[0]
        super(ChoiceList, self).__init__(
            random_klass, tagname=None, nillable=nillable, namespace=namespace
        )
        self._class_mapping = class_mapping
        self._elements = dict() #{self: 'parenteralia_antworten'})
        for type_, name in class_mapping.items():
            key = xsd.Element(type_, tagname=name)
            key._name = name
            self._elements[key] = name
        self._maxOccurs = maxOccurs
        self._minOccurs = minOccurs

    def _evaluate_type(self):
        self._type = self
        for element in self._elements:
            if element == self:
                continue
            element._evaluate_type()

    def xsd_elements(self):
        return self._elements

    # -------------------------------------------------------------------------
    # initially copied from soapfish/xsd.py (but with a few custom modifications)
    @classmethod
    def _is_matching_element(cls, field, xmlelement):
        def gettagns(tag):
            '''
            Translates a tag string in a format {namespace} tag to a tuple
            (namespace, tag).
            '''
            if tag[0] == '{':
                return tag[1:].split('}', 1)
            else:
                return (None, tag)
        if isinstance(xmlelement, etree._Comment):
            return False
        ns, tag = gettagns(xmlelement.tag)
        return (tag == field._name) or (tag == field.tagname)

    def parse(self, instance, field_name, xmlelement):
        self._evaluate_type()
        if xmlelement.get('{%s}nil' % xsd.ns.xsi) == 'true':
            value = xsd.NIL
        else:
            for choice_element in self.xsd_elements():
                if self._is_matching_element(choice_element, xmlelement):
                    value = choice_element._type.parse_xmlelement(xmlelement)
                    break
            else:
                raise AssertionError('can not parse %r' % xmlelement.tag)
        getattr(instance, field_name).append(value)

    def render(self, parent, field_name, value, namespace=None, elementFormDefault=None):
        self._evaluate_type()
        items = value  # The value must be list of items.
        if self._minOccurs and len(items) < self._minOccurs:
            raise ValueError('For %s minOccurs=%d but list length %d.' % (field_name, self._minOccurs, len(items)))
        if self._maxOccurs and len(items) > self._maxOccurs:
            raise ValueError('For %s maxOccurs=%d but list length %d.' % (field_name, self._maxOccurs, len(items)))

        if self.namespace is not None:
            namespace = self.namespace

        for item in items:
            tagname = self._tagname_for_item(item, namespace, elementFormDefault)
            xmlelement = etree.Element(tagname)
            if item == xsd.NIL:
                xmlelement.set('{%s}nil' % xsd.ns.xsi, 'true')
            else:
                item.render(xmlelement, item, namespace=namespace, elementFormDefault=elementFormDefault)
            parent.append(xmlelement)

    def _tagname_for_item(self, item, namespace, elementFormDefault):
        tagname = self._class_mapping[item.__class__]
        if (namespace is not None) and (elementFormDefault == xsd.ElementFormDefault.QUALIFIED):
            return '{%s}%s' % (namespace, tagname)
        return tagname
    # --- end of copy ---------------------------------------------------------
