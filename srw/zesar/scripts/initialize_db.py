#!/usr/bin/env python

from __future__ import absolute_import

import os
import sys

from pyramid.paster import get_appsettings, setup_logging
from sqlalchemy import engine_from_config

# this script might be called directly so relative imports won't work.
from srw.zesar.models import metadata, DBSession


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri>\n(example: "%s production.ini")' % (cmd, cmd))
    sys.exit(1)


def main(argv=sys.argv):
    if len(argv) != 2:
        usage(argv)
    config_uri = argv[1]
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    metadata.create_all(engine)

if __name__ == '__main__':
    main()
