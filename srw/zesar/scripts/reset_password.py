#!/usr/bin/env python

from __future__ import absolute_import, unicode_literals

import sys

from argparse import ArgumentParser
from pyramid.paster import setup_logging

# this script might be called directly so relative imports won't work.
from srw.zesar.auth import auth_manager_from_config
from srw.zesar.models import DBSession, User
from srw.zesar.pyramid_app import configure_db
from srw.zesar.scripts.add_user import (
    get_password_from_certificate,
    load_settings,
)


def main(argv=sys.argv):
    parser = ArgumentParser(description='reset user password for SRW ZESAR.')
    parser.add_argument('config_filename')
    parser.add_argument('--username', dest='username')

    auth_group = parser.add_mutually_exclusive_group(required=True)
    auth_group.add_argument('--certificate', dest='certificate')
    auth_group.add_argument('--password', dest='password')

    args = parser.parse_args()
    if not args.username:
        print('Error: please specify a username!')
        sys.exit(1)

    site_config = load_settings(args.config_filename)
    setup_logging(args.config_filename)
    settings = site_config.to_dict('app:main')
    configure_db(settings)

    user = User.query.filter(User.username == args.username).first()
    if user is None:
        print('Error: no such user %r' % args.username)
        sys.exit(2)
    password = args.password
    if args.certificate:
        password = get_password_from_certificate(args.certificate)

    auth_manager = auth_manager_from_config(settings)
    user.password_hash = auth_manager.compute_hash(password)
    DBSession.add(user)
    DBSession.commit()

if __name__ == '__main__':
    main()
