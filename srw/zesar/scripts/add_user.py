#!/usr/bin/env python

from __future__ import absolute_import, unicode_literals

import binascii
import os
import ssl
import sys

from argparse import ArgumentParser
from pyramid.paster import setup_logging
import six

# this script might be called directly so relative imports won't work.
from srw.zesar.auth import auth_manager_from_config
from srw.zesar.lib.multiconfig import SiteConfig
from srw.zesar.models import DBSession, User
from srw.zesar.pyramid_app import configure_db


def load_settings(config_filenames, here_dir=None):
    if isinstance(config_filenames, six.string_types):
        config_filenames = [config_filenames]
    if here_dir is None:
        here_dir = os.getcwd()
    config_sources = [('srw.zesar', 'application.ini')] + list(config_filenames)
    mandatory_sources = filter(None, config_sources)
    interpolations = {'here': here_dir}
    site_config = SiteConfig(mandatory=mandatory_sources, interpolations=interpolations)
    return site_config

def get_password_from_certificate(certificate):
    if not os.path.exists(certificate):
        print('Error: certificate file does not exist (%s)' % certificate)
        sys.exit(2)
    with open(certificate, 'r') as cert_fp:
        pem_cert = cert_fp.read()
    try:
        der_cert = ssl.PEM_cert_to_DER_cert(pem_cert)
        # bcrypt algorithm does not allow null bytes
        password = binascii.hexlify(der_cert)
    except Exception as e:
        print('Error: unable to parse certificate (%s)' % e)
        sys.exit(2)
    return password


def main(argv=sys.argv):
    parser = ArgumentParser(description='add a user for srw ZESAR.')
    parser.add_argument('config_filename')
    parser.add_argument('--username', dest='username')

    auth_group = parser.add_mutually_exclusive_group(required=True)
    auth_group.add_argument('--certificate', dest='certificate')
    auth_group.add_argument('--password', dest='password')

    role_group = parser.add_mutually_exclusive_group(required=True)
    role_group.add_argument('--is-zesar', dest='is_zesar', action='store_true')
    role_group.add_argument('--is-srw', dest='is_srw', action='store_true')

    args = parser.parse_args()
    if not args.username:
        print('Error: please specify a username!')
        sys.exit(1)

    site_config = load_settings(args.config_filename)
    setup_logging(args.config_filename)
    settings = site_config.to_dict('app:main')
    configure_db(settings)

    username = args.username
    password = args.password
    if args.certificate:
        password = get_password_from_certificate(args.certificate)

    auth_manager = auth_manager_from_config(settings)
    password_hash = auth_manager.compute_hash(password)
    user = User(username=username, password_hash=password_hash,
        is_zesar=args.is_zesar,
        is_admin=args.is_srw
    )
    DBSession.add(user)
    DBSession.commit()

if __name__ == '__main__':
    main()
