
from __future__ import absolute_import

from pyramid.response import Response
from pyramid.view import view_config
from soapfish.core import SOAPRequest
from soapfish.soap_dispatch import SOAPDispatcher

from .admin.service import AdminService


@view_config(route_name='admin_service')
@view_config(route_name='admin_wsdl')
def soap_dispatch(request):
    content_type = 'text/xml; charset="utf-8"'
    soap_environ = request.environ.copy()
    soap_environ['settings'] = request.registry.settings
    soapfish_request = SOAPRequest(soap_environ, request.body)
    soap_response = SOAPDispatcher(AdminService).dispatch(soapfish_request)
    soapxml_body = soap_response.http_content
    response = Response(
        status=soap_response.http_status_code,
        content_type=content_type,
        body=soapxml_body,
    )
    return response


