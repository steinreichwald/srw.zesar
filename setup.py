#!/usr/bin/env python

import re
import sys

from setuptools import setup

if sys.version_info < (2, 6):
    sys.stderr.write('SRWZesar needs at least Python 2.6')
    sys.exit(1)


def requires_from_file(filename):
    requirements = []
    with open(filename, 'r') as requirements_fp:
        for line in requirements_fp.readlines():
            match = re.search('^\s*([a-zA-Z][^#]+?)(\s*#.+)?\n$', line)
            if match:
                requirements.append(match.group(1))
    return requirements


setup(
    test_suite='srw.zesar',

    tests_require = requires_from_file('dev_requirements.txt'),
    install_requires=requires_from_file('requirements.txt'),
    entry_points="""
        [paste.app_factory]
        main = srw.zesar.pyramid_app:make_app

        [console_scripts]
        zesar-dbinit = srw.zesar.scripts.initialize_db:main
        zesar-create-user = srw.zesar.scripts.add_user:main
        zesar-reset-password = srw.zesar.scripts.reset_password:main
    """,
)

